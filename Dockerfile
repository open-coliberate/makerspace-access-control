FROM resin/raspberrypi3-node:8

RUN sed -i '/jessie-updates/d' /etc/apt/sources.list  # Now archived

##### Python 3.6 Install

# remove several traces of debian python
RUN apt-get purge -y python.*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8

# key 63C7CC90: public key "Simon McVittie <smcv@pseudorandom.co.uk>" imported
# key 3372DCFA: public key "Donald Stufft (dstufft) <donald@stufft.io>" imported
RUN gpg --keyserver keyring.debian.org --recv-keys 4DE8FF2A63C7CC90 \
  && gpg --keyserver keyserver.ubuntu.com --recv-key 6E3CBCE93372DCFA \
  && gpg --keyserver keyserver.ubuntu.com --recv-keys 0x52a43a1e4b77b059

# Install other apt deps
RUN apt-get update && apt-get install -y --no-install-recommends \
  apt-utils \
  clang \
  cron \
  xserver-xorg-core \
  xserver-xorg-input-all \
  xserver-xorg-video-fbdev \
  xorg \
  libxcb-image0 \
  libxcb-util0 \
  xdg-utils \
  libdbus-1-dev \
  libdbus-glib-1-dev \
  libgtk2.0-dev \
  libnotify-dev \
  libgnome-keyring-dev \
  libgconf2-dev \
  libasound2-dev \
  libcap-dev \
  libcups2-dev \
  libxtst-dev \
  libxss1 \
  libnss3-dev \
  libsmbclient \
  libssh-4 \
  fbset \
  chromium-browser \
  vim \
  unzip \
  zip \
  htop \
  unclutter \
  dnsmasq \
  wireless-tools \
  gcc \
  wget \
  libexpat-dev && rm -rf /var/lib/apt/lists/*


ENV PYTHON_VERSION 3.6.1

# if this is called "PIP_VERSION", pip explodes with "ValueError: invalid truth value '<VERSION>'"
ENV PYTHON_PIP_VERSION 9.0.1

ENV SETUPTOOLS_VERSION 34.3.3

RUN set -x \
  && curl -SLO "http://resin-packages.s3.amazonaws.com/python/v$PYTHON_VERSION/Python-$PYTHON_VERSION.linux-armv6hf.tar.gz" \
  && echo "85e7295f5194ee2c099cfcf2a27e186d9714e849468bf1fa52292be6b2217621  Python-3.6.1.linux-armv6hf.tar.gz" | sha256sum -c - \
  && tar -xzf "Python-$PYTHON_VERSION.linux-armv6hf.tar.gz" --strip-components=1 \
  && rm -rf "Python-$PYTHON_VERSION.linux-armv6hf.tar.gz" \
  && ldconfig \
  && if [ ! -e /usr/local/bin/pip3 ]; then : \
    && curl -SLO "https://raw.githubusercontent.com/pypa/get-pip/430ba37776ae2ad89f794c7a43b90dc23bac334c/get-pip.py" \
    && echo "19dae841a150c86e2a09d475b5eb0602861f2a5b7761ec268049a662dbd2bd0c  get-pip.py" | sha256sum -c - \
    && python3 get-pip.py \
    && rm get-pip.py \
  ; fi \
  && pip3 install --no-cache-dir --upgrade --force-reinstall pip=="$PYTHON_PIP_VERSION" setuptools=="$SETUPTOOLS_VERSION" \
  && [ "$(pip list |tac|tac| awk -F '[ ()]+' '$1 == "pip" { print $2; exit }')" = "$PYTHON_PIP_VERSION" ] \
  && find /usr/local \
    \( -type d -a -name test -o -name tests \) \
    -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
    -exec rm -rf '{}' + \
  && cd / \
  && rm -rf /usr/src/python ~/.cache

# install "virtualenv", since the vast majority of users of this image will want it
RUN pip3 install --no-cache-dir virtualenv

ENV PYTHON_DBUS_VERSION 1.2.4

# install dbus-python
RUN set -x \
  && mkdir -p /usr/src/dbus-python \
  && curl -SL "http://dbus.freedesktop.org/releases/dbus-python/dbus-python-$PYTHON_DBUS_VERSION.tar.gz" -o dbus-python.tar.gz \
  && curl -SL "http://dbus.freedesktop.org/releases/dbus-python/dbus-python-$PYTHON_DBUS_VERSION.tar.gz.asc" -o dbus-python.tar.gz.asc \
  && gpg --verify dbus-python.tar.gz.asc \
  && tar -xzC /usr/src/dbus-python --strip-components=1 -f dbus-python.tar.gz \
  && rm dbus-python.tar.gz* \
  && cd /usr/src/dbus-python \
  && PYTHON=python3.6 ./configure \
  && make -j$(nproc) \
  && make install -j$(nproc) \
  && cd / \
  && rm -rf /usr/src/dbus-python

# make some useful symlinks that are expected to exist
RUN cd /usr/local/bin \
  && ln -sf pip3 pip \
  && { [ -e easy_install ] || ln -s easy_install-* easy_install; } \
  && ln -sf idle3 idle \
  && ln -sf pydoc3 pydoc \
  && ln -sf python3 python \
  && ln -sf python3-config python-config

VOLUME /data

# Does this need to be here? are there other ports i need to expose?
EXPOSE 6379

### Setup python app
WORKDIR /usr/src/app/swipesystem

COPY ./swipesystem ./

RUN pip install -r requirements.txt

# Install supervisor to manage processes
# RUN pip install git+https://github.com/Supervisor/supervisor.git
RUN pip install https://github.com/Supervisor/supervisor/archive/master.zip

# Start apps
WORKDIR /usr/src/app

COPY ./kiosk.sh ./
COPY ./supervisor.conf ./

## sqlite backup fstab
COPY ./swipesystem/src/services/media-sqlite_backup.mount /etc/systemd/system/
COPY ./swipesystem/src/services/media-makerspace_access_control_usb.mount /etc/systemd/system/
RUN mkdir -p /media/sqlite_backup \
    && mkdir -p /media/makerspace_access_control_usb \
    && systemctl enable media-makerspace_access_control_usb.mount \
    && systemctl enable media-sqlite_backup.mount

## setup usb services
COPY ./swipesystem/src/services/mactlusb.service /etc/systemd/system/
COPY ./swipesystem/src/services/sqlite_backup.service /etc/systemd/system/
COPY ./swipesystem/src/services/mactlusb /bin/
COPY ./swipesystem/src/services/backup_sqlite_db /bin/
RUN chmod +x /bin/mactlusb /bin/backup_sqlite_db \
    && systemctl enable mactlusb.service \
    && systemctl enable sqlite_backup.service

## sqlite backup crontab
COPY ./jobs.txt ./
RUN echo 'write /etc/crontab' \
    && crontab /usr/src/app/jobs.txt

### chrome keyboard extension
RUN git clone -q https://github.com/jmbott/chrome-virtual-keyboard.git virtual-keyboard \
  && sed -i 's/openUrl(chrome.extension.getURL("options.html"));/\/\/&/' virtual-keyboard/script.js

ENV DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

RUN wget https://github.com/balena-io/wifi-connect/releases/download/v4.1.1/wifi-connect-v4.1.1-linux-rpi.tar.gz

RUN tar xvfz wifi-connect-v4.1.1-linux-rpi.tar.gz

# Use apt-get to install dependencies
RUN apt-get update && apt-get install -yq --no-install-recommends \
    python-dev \
    python-smbus \
    python-psutil \
    wireless-tools && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

## uncomment if you want systemd
ENV INITSYSTEM on

ENV FILEREADER_FP=/tmp/cards.txt
ENV OUTFILE=/tmp/cards.txt
ENV EVDEV_READER_RESULTS_FILE=/tmp/cards.txt
ENV OUTPUT_STATUS_FILE=/tmp/output-status.json
ENV SQLITE_DB=/data/swipe.db

RUN export SQLITE_DB=/data/swipe.db \
    && python3 swipesystem/src/db.py

# Start app
CMD ["supervisord", "-c", "supervisor.conf"]
