import os
import sys


if __name__ == '__main__':
    with open(os.environ.get('EVDEV_READER_RESULTS_FILE'), 'ab', 0) as f:
        card_id = sys.argv[1] + '\n'
        f.write(card_id.encode())
        print('Wrote card value:', card_id)
