import asyncio


class EventBus(object):
    def __init__(self, loop=None):
        self.loop = loop or asyncio.get_event_loop()
        self.listeners = []

    def emit_nowait(self, event):
        asyncio.ensure_future(self.emit(event), loop=self.loop)

    # TODO: make this a blocking emit, ie. await all listeners
    async def emit(self, event):
        for listener in self.listeners:
            asyncio.ensure_future(listener(event))

    def register_listener(self, listener : callable):
        self.listeners.append(listener)


bus = EventBus()
