""""
Ensure you have a you have a redis db running on port 6379 with data from the old system.
Run: SQLITE_DB=/path/to/db.sqlite python3 import_redis_data.py
"""

import os
from typing import List
from pprint import pprint as pp

import redis

import db

rdb = redis.Redis(host='localhost', port=6379, db=0)

engine = db.get_engine(os.environ.get('SQLITE_DB'))
db.create_tables(engine)

session = db.get_session(engine)


def get_users_from_redis():
    user_keys = rdb.keys('user.*')
    users = [rdb.hgetall(key) for key in user_keys]
    pp(users)
    return users

def create_users_in_sqlite(users : List[dict]):
    """
    [{b'first_name': b'William',
    b'is_admin': b'true',
    b'last_name': b'Foo',
    b'uid': b'1243\n456'},

    {b'first_name': b'MARIA',
    b'is_admin': b'false',
    b'last_name': b'Bar',
    b'uid': b'789\123'}]
    """

    for i, user in enumerate(users):
        user_object = db.User(
            first_name=user.get(b'first_name', b'N/A').decode(),
            last_name=user.get(b'last_name', b'N/A').decode(),
            card_uid=user.get(b'uid', b'N/A').decode().replace('\n', '--')
        )

        if user_object.card_uid == 'N/A':
            print('Skipping user (no card uid)', user)
            continue

        if user_object.first_name in ('N/A', '') and user_object.last_name in ('N/A', ''):
            print('Skipping user (no name)')
            continue

        try:
            session.add(user_object)
            session.commit()
        except Exception as e:
            print('Failed to create user', e)
            session.flush()


def main():
    users = get_users_from_redis()
    create_users_in_sqlite(users)

if __name__ == '__main__':
    main()