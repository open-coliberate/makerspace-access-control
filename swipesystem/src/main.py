import asyncio

import os
from signal import SIGINT, signal
from spf import SanicPluginsFramework

import web
from bus import bus

from services import card_reader_service, output_status_service

spf = SanicPluginsFramework(web.app)

# async def time():
#     import datetime
#     while True:
#         # print(datetime.datetime.now())
#         await bus.emit(datetime.datetime.now())
#         await asyncio.sleep(1)


def register_services(loop, event_bus):
    card_reader = card_reader_service.CardReaderService(
        event_bus, loop=loop
    )
    # loop.create_task(card_reader.start())
    asyncio.ensure_future(card_reader.start(), loop=loop)

    output_status = output_status_service.OutputStatusService(
        event_bus, loop=loop
    )
    asyncio.ensure_future(output_status.start(), loop=loop)


def shutdown(loop):
    print('shutting down')
    pending = asyncio.Task.all_tasks()
    for task in pending:
        task.cancel()
    loop.stop()


def run():
    loop = asyncio.get_event_loop()
    server = web.app.create_server(
        host='0.0.0.0',
        port=8000,
        return_asyncio_server=True,
        asyncio_server_kwargs=None,
        debug=os.environ.get('DEBUG')
    )
    asyncio.ensure_future(server)
    spf._on_server_start(web.app, loop)
    register_services(loop, bus)

    # asyncio.get_event_loop().create_task(time())

    signal(SIGINT, lambda s, f: shutdown(loop))
    try:
        loop.run_forever()
    except:
        loop.stop()


if __name__ == '__main__':
    print('Starting new swipesystsem')
    run()
