import datetime as dt
import os

import sqlalchemy
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    CheckConstraint
)
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.orm import relationship, sessionmaker


Base = declarative_base()


class User(Base):
    """
    user, super user, lab user
    """
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String, unique=True)
    card_uid = Column(String)
    is_admin = Column(Boolean)
    role = Column(String)
    first_name = Column(String)
    last_name = Column(String)

    def get_display_name(self):
        # TODO: user fname + lname
        if self.first_name or self.last_name:
            return f'{self.first_name or ""} {self.last_name or ""}'
        return self.email

    __table_args__ = (
        # could put card regex here in future
        CheckConstraint('length(card_uid) > 0'),
    )


class ToolProfile(Base):
    __tablename__ = 'tool_profile'

    id = Column(Integer, primary_key=True)
    uuid = Column(String, unique=True)  # from the web
    profile_name = Column(String, unique=True)  # TODO: rename this to just 'name'
    power_on_seconds = Column(Integer)
    warn_at_seconds = Column(Integer)
    requires_training = Column(Boolean)
    restricted_to_open_hours = Column(Boolean)
    enable_warning_sound = Column(Boolean)
    buddy_mode = Column(Boolean)


class Tool(Base):
    __tablename__ = 'tool'
    id = Column(Integer, primary_key=True)
    device_id = Column(String, unique=True)
    tool_profile_id = Column(Integer, ForeignKey('tool_profile.id'))
    uuid = Column(String, unique=True) # from the web

    tool_profile = relationship(ToolProfile)

    # tool_profile = relationship('ToolProfile', foreign_keys='ToolProfile.id')

    @property
    def profile_name(self):
        return self.tool_profile.profile_name if self.tool_profile else 'N/A'


class ToolReservation(Base):
    __tablename__ = 'tool_reservation'
    id = Column(Integer, primary_key=True)
    tool_id = Column(Integer, ForeignKey('tool.id'))
    start_time = Column(DateTime)
    duration_seconds = Column(Integer)
    extension_duration_seconds = Column(Integer, default=0)
    user_id = Column(Integer, ForeignKey('user.id'))
    uuid = Column(String)
    is_cancelled = Column(Boolean)
    cancellation_time = Column(DateTime)

    tool = relationship(Tool)
    user = relationship(User)

    @hybrid_property
    def end_time(self):
        return (
            self.start_time +
            dt.timedelta(seconds=self.duration_seconds) +
            dt.timedelta(seconds=self.extension_duration_seconds or 0)
        )


# TODO: define swipe events
class SwipeEvent(Base):
    __tablename__ = 'swipe_event'
    id = Column(Integer, primary_key=True)
    # timestamp
    # userid (card id)


def get_engine(db_path=':memory:'):
    db_uri = sqlalchemy.engine.url.URL(**{
        'drivername': 'sqlite',
        'database': db_path
    })
    # return create_engine(('sqlite://' if ':memory:' in db_path else db_uri))
    return create_engine(db_uri)


engine = get_engine(os.environ.get('SQLITE_DB'))


def create_tables(engine):
    Base.metadata.create_all(engine)

def drop_tables(engine):
    Base.metadata.drop_all(engine)


session = None
def get_session(engine):
    global session
    if not session:
        Session = sessionmaker(bind=engine)
        session = Session()
    return session


if __name__ == '__main__':
    engine = get_engine(os.environ.get('SQLITE_DB'))
    create_tables(engine)
