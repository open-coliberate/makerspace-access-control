import asyncio
import atexit
import os
import time

import evdev
from evdev import InputDevice, categorize, ecodes

import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__file__)


SCANCODES = {
    # Scancode: ASCIICode
    0: None, 1: 'ESC', 2: '1', 3: '2', 4: '3', 5: '4', 6: '5', 7: '6', 8: '7', 9: '8',
    10: '9', 11: '0', 12: '-', 13: '=', 14: 'BKSP', 15: 'TAB', 16: 'Q', 17: 'W', 18: 'E', 19: 'R',
    20: 'T', 21: 'Y', 22: '', 23: 'I', 24: 'O', 25: 'P', 26: '[', 27: ']', 28: '--', 29: 'LCTRL',
    30: 'A', 31: 'S', 32: 'D', 33: 'F', 34: 'G', 35: 'H', 36: 'J', 37: 'K', 38: 'L', 39: ';',
    40: '"', 41: '`', 42: 'LSHFT', 43: '\\', 44: 'Z', 45: 'X', 46: 'C', 47: 'V', 48: 'B', 49: 'N',
    50: 'M', 51: ',', 52: '.', 53: '/', 54: 'RSHFT', 56: 'LALT', 100: 'RALT'
}


def get_omnikey_device():
    devices = []

    try:
        devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    except OSError:
        pass

    # Filter for devices for omnikey
    omnikey_devices = [dev for dev in devices if 'HID OMNIKEY' in dev.name]

    log.debug(f'Detected omnikey devices: {omnikey_devices}')

    return omnikey_devices[0] if omnikey_devices else None


async def read_from_device(event_queue):
    while True:
        omnikey_device = get_omnikey_device()

        if not omnikey_device:
            log.debug('No omnikey devices detected')
            await asyncio.sleep(1)
            continue

        try:
            await read_device_events(omnikey_device, event_queue)
        except OSError:
            log.info('Device disconnected')
            pass


async def read_device_events(device, event_queue):
    log.info('Listening for card data')

    device.grab()  # Become the sole recipient of this device
    atexit.register(device.ungrab)

    async for event in device.async_read_loop():
        if event.type == ecodes.EV_KEY:
            data = categorize(event)

            if data.keystate == 1:  # Down events only
                ascii_char = SCANCODES.get(data.scancode)
                event_queue.put_nowait(ascii_char)


async def parse_device_messages(event_queue, parsed_data_queue):
    """
    Reads omnikey data via the following algorithm,
    1. Read first data item
    2. Wait N seconds
    3. Read remaining data items from queue as 1 message
    """

    while True:
        res = ''
        res = await event_queue.get()
        await asyncio.sleep(1)
        while not event_queue.empty():
            res += event_queue.get_nowait()
        log.info(f'Obtained device data: {res}')
        parsed_data_queue.put_nowait(res)


async def file_writer(file_path, parsed_data_queue):
    with open(file_path, 'ab', 0) as f:
        while True:
            card_data = await parsed_data_queue.get()
            log.debug(f'Writing to file: {card_data}')
            f.write(f'{card_data}\n'.encode())


async def main():
    event_queue = asyncio.Queue()
    parsed_data_queue = asyncio.Queue()
    outfile = os.environ.get('OUTFILE', 'evdev_reader_results.txt')
    await asyncio.gather(
        read_from_device(event_queue),
        parse_device_messages(event_queue, parsed_data_queue),
        file_writer(outfile, parsed_data_queue)
    )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
