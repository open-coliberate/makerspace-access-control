import asyncio
import concurrent.futures
import io
import logging
import os
import json

from time import sleep, time
import datetime as dt

from web import helpers

from gpiozero import LED

MOCK_DEVICE = (bool(os.environ.get('MOCK_DEVICE')) or False)
if MOCK_DEVICE == True:
    from gpiozero.pins.mock import MockFactory
    from gpiozero import Device
    # Set the default pin factory to a mock factory
    Device.pin_factory = MockFactory()

LOG = logging.getLogger(__file__)
LOG.setLevel(logging.DEBUG)


class OutputStatusService(object):
    """For Signaling Output On/Off"""

    def __init__(self, event_bus, loop=None):
        self.loop = loop or asyncio.get_event_loop()
        self.event_bus = event_bus
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.resp = None
        self.output_status = None
        self.current_status = None
        self.local = False
        self.output = LED(17)

    async def start(self):
        # register task in event bus which fires events
        print('[OutputStatusService] Starting.')
        asyncio.ensure_future(self._start(), loop=self.loop)
        print('[OutputStatusService] Started.')

    async def _start(self):
        while True:
            try:
                self.resp = await self.loop.run_in_executor(
                    self.executor, self.update_output
                )
                if not self.resp:
                    continue

                self.output.value = self.output_status
                self.current_status = self.output_status

                # LOG.debug(f'[OutputStatusService] End reservation: {end_card_id}.')
                print(f'[OutputStatusService] Output Status Changed: {self.current_status}.')
                event = {
                    'type': 'output-status',
                    'payload': {'outputStatus': self.current_status}
                }

                self.event_bus.emit_nowait(event)
                print(f'[OutputStatusService] Emitted event: {event}.')

            except Exception as e:
                print(e)
                print('Failed to read switch signal and emit to event bus')


    def update_output(self):
        """Update output status value."""

        self.output_status = helpers.read_output_status().get('outputStatus')
        #print(self.output_status, self.current_status)
        if self.output_status == self.current_status:
            sleep(.5)
            return
        else:
            self.local = False

        return True


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    event_bus = None
    reader = OutputStatusService(loop, None)
