import asyncio
import concurrent.futures
import io
import logging
import os
import re
from time import sleep, time

FILEREADER_FP = os.environ['FILEREADER_FP']
LOG = logging.getLogger(__file__)
LOG.setLevel(logging.DEBUG)


class CardReaderService(object):
    """Assume card reader appends to a file.  Tail bytes from that file"""

    def __init__(self, event_bus, loop=None, filereader_fp=None):
        self.loop = loop or asyncio.get_event_loop()
        self.event_bus = event_bus
        self.filereader_fp = filereader_fp or FILEREADER_FP
        self.seek_pos = None
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

    async def start(self):
        # register task in event bus which fires events
        print('[CardReaderService] Starting.')
        asyncio.ensure_future(self._start(), loop=self.loop)
        print('[CardReaderService] Started.')

    async def _start(self):
        while True:
            try:
                card_id = await self.loop.run_in_executor(
                    self.executor, self.read
                )
                if not card_id:
                    continue

                # LOG.debug(f'[CardReaderService] Read card id: {card_id}.')
                print(f'[CardReaderService] Read card id: {card_id}.')
                event = {'type': 'card-swipe', 'payload': {'card_id': card_id}}

                self.event_bus.emit_nowait(event)
                print(f'[CardReaderService] Emitted event: {event}.')
            except Exception as e:
                print(e)
                print('Failed to read card value and emit to event bus')

    def read(self) -> str:
        """
        Reads last line in the file and returns the value.
        This method will block if it has read the last line and wait until a
        new line is written.
        """

        # Wait until file exists and has data
        is_reader_file_missing = lambda : not os.path.exists(self.filereader_fp)
        is_reader_file_empty = lambda: os.path.getsize(self.filereader_fp) == 0
        while is_reader_file_missing() or is_reader_file_empty():
            sleep(.5)
            return

        with open(self.filereader_fp, 'rb') as f:
            while True:
                f.seek(-2, os.SEEK_END)      # Jump to the 2nd to last line

                if f.tell() == self.seek_pos:  # Have we read from this position before?
                    sleep(0.1)                 # wait before checking if new line was written
                    return                   # go back to the top

                self.seek_pos = f.tell()       # mark that we've read up to this part of the file

                while f.tell() != 0 and f.read(1) != b'\n':    # check if current byte is a line ending
                    f.seek(-2, os.SEEK_CUR)  # Read back the read byte and 1 more byte

                return f.readlines()[-1].decode()     # read the last line


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    event_bus = None
    reader = CardReaderService(loop, None)
    # TODO: implement starting card reader service here...
