import os
os.environ['SQLITE_DB'] = '/tmp/swipe-test.db'
os.environ['DEVICE_CONFIG_FILE'] = '/tmp/swipe-test-config.json'

import datetime as dt
import json

from sanic import Sanic
from sanic.testing import PORT as PORT_BASE, SanicTestClient
import pytest

from web import routes, views  # NOQA
from web import helpers

# TODO: clean up

import db


def init_device_configs():
    helpers.write_device_configs({
        'tool_uuid': '123',
        'organization': 'test-org',
    })


def setup_data():
    engine = db.get_engine('/tmp/swipe-test.db')
    session = db.get_session(engine)
    user = db.User(email='foo@bar.com', card_uid='456')
    tool_profile = db.ToolProfile(
        power_on_seconds=5*60,
        warn_at_seconds=5,
        profile_name='printer'
    )
    tool = db.Tool(
        tool_profile=tool_profile,
        uuid='123'
    )

    session.add(user)
    session.add(tool_profile)
    session.add(tool)
    session.commit()


@pytest.fixture(scope="function")
def app():
    init_device_configs()

    engine = db.get_engine('/tmp/swipe-test.db')
    db.drop_tables(engine)
    db.create_tables(engine)

    setup_data()

    app = Sanic()

    routes.configure_routes(app)
    return app


@pytest.fixture(scope="function")
def client(app):
    return SanicTestClient(app, 8585)


def test_get_device_info(client):
    _, response = client.get('/device-info')
    assert response.status == 200
    assert 'organization' in response.json
    assert 'is_configured' in response.json
    assert 'device_id' in response.json


def test_configure_organization(client):
    _, resp = client.post('/configure-organization', data=json.dumps({}))
    assert resp.status == 200
    # assert 'organization' in response.json
    # assert 'is_configured' in response.json
    # assert 'machine_id' in response.json


def test_get_tool_rervation(client):
    _, resp = client.get('/tool-reservation')
    assert resp is not None
    assert resp.status == 200
    assert resp.json == {}

    tool = db.session.query(db.Tool).first()
    user = db.session.query(db.User).first()
    tool_profile = db.session.query(db.ToolProfile).first()
    reservation = db.ToolReservation(
        tool=tool,
        user=user,
        start_time=dt.datetime.now(),
        duration_seconds=tool_profile.power_on_seconds
    )
    db.session.add(reservation)
    db.session.commit()

    _, resp = client.get('/tool-reservation')
    assert resp is not None
    assert resp.status == 200
    assert resp.json['user'] == user.get_display_name()
    assert resp.json['end_time'] == reservation.end_time.isoformat()


def test_create_tool_rervation(client):
    _, resp = client.get('/tool-reservation')
    assert resp is not None
    assert resp.status == 200
    assert resp.json == {}

    # Create tool reservation
    data = {'cardUid': '456', 'action': 'create'}
    _, resp = client.post(
        '/tool-reservation', data=json.dumps(data))
    assert resp.status == 200
    assert resp.json['status'] == 'success'

    reservation = db.session.query(db.ToolReservation).first()
    assert reservation is not None


def test_extend_reservation(client):
    _, resp = client.get('/tool-reservation')
    assert resp is not None
    assert resp.status == 200
    assert resp.json == {}

    # Create tool reservation
    data = {'cardUid': '456', 'action': 'create'}
    _, resp = client.post(
        '/tool-reservation', data=json.dumps(data))
    assert resp.status == 200
    assert resp.json['status'] == 'success'

    _, resp = client.get('/tool-reservation')
    initial_end =  resp.json['end_time']

    reservation = db.session.query(db.ToolReservation).first()
    initial_reservation_end_time = reservation.end_time.timestamp()
    assert reservation is not None

    data = {'cardUid': '456', 'action': 'extend'}
    _, resp = client.post(
        '/tool-reservation', data=json.dumps(data))

    # HACK: to fix issue below
    _, resp = client.get('/tool-reservation')
    new_end =  resp.json['end_time']
    assert new_end != initial_end


    # TODO: debug why this assertion fails, why are ORM results not diff?
    # reservation = db.session.query(db.ToolReservation).first()
    # assert reservation.end_time.timestamp() > initial_reservation_end_time
