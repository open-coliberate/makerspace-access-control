# python tests

Run python tests with `pytest` in this directory.

* the app must be running, use `start_local.sh`
* its recommended to use a python virtualenv with all requirements installed
  * if you ran the startup script then, `source ../../../venv/bin/activate`
  * to reinstall the requirements, `pip install -r ../../requirements.txt`
