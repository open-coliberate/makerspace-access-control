import os

import pytest

import datetime as dt

from db import *


@pytest.yield_fixture(scope='function')
def session():
    engine = get_engine('/tmp/swipe-test.db')
    drop_tables(engine)
    create_tables(engine)
    yield get_session(engine)
    drop_tables(engine)


def test_create_reservation(session):
    user = User(email='foo@bar.com')
    tool_profile = ToolProfile(power_on_seconds=15, warn_at_seconds=5)
    tool = Tool(
        tool_profile=tool_profile
    )
    reservation = ToolReservation(
        start_time=dt.datetime.now(),
        duration_seconds=tool_profile.power_on_seconds
    )

    session.add(user)
    session.add(tool_profile)
    session.add(tool)
    session.add(reservation)
    session.commit()

    res_delta = (reservation.end_time - reservation.start_time).seconds
    assert  res_delta == tool_profile.power_on_seconds

    assert len(session.query(ToolReservation).all()) == 1
