import string
import random
import datetime as dt

import db


# ref: https://codereview.stackexchange.com/questions/58269/generating-random-email-addresses
domains = [ "hotmail.com", "gmail.com", "aol.com", "mail.com" , "mail.kz", "yahoo.com"]
letters = string.ascii_lowercase[:12]

def get_random_domain(domains):
    return random.choice(domains)

def get_random_name(letters, length):
    return ''.join(random.choice(letters) for i in range(length))

def generate_random_emails(nb, length):
    return [get_random_name(letters, length) + '@' + get_random_domain(domains) for i in range(nb)]


def test_create_users(session):
    for i in range(50):
        user = db.User(email=generate_random_emails(1,6)[0])
        session.add(user)
        session.commit()


def main():
    try:
        engine = db.get_engine('/tmp/swipe.db') # or /tmp/swipe-test.db for pytests
        session = db.get_session(engine)
        test_create_users(session)
        print("created 50 users")
    except Exception as e:
        error = str(e)


if __name__ == "__main__":
    main()
