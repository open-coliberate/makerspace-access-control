import socketio
import logging

from bus import bus


LOG = logging.getLogger(__file__)


# TODO: make event bus an arg
class FeedNamespace(socketio.AsyncNamespace):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        bus.register_listener(self.feed_send)

    async def feed_send(self, event):
        LOG.debug(f'Forwarding event: {event}')
        await self.emit('data', event)

    def on_connect(self, sid, environ):
        print('Connection started', sid, environ)

    def on_disconnect(self, sid):
        print('disconnected', sid)
    
    async def on_event(self, sid, data):
        bus.emit(data)
        # await self.emit('foo')
