import asyncio

from web import views


def configure_routes(app):
    app.add_route(views.status, '/status')
    app.add_route(views.get_device_info, '/device-info')
    app.add_route(views.user_has_email, '/user-has-email')
    app.add_route(views.wifi_connect, '/wifi-connect')
    app.add_route(
        views.get_wifi_info,
        '/get-wifi-info',
        methods=['POST']
    )
    app.add_route(
        views.has_admin_permissions,
        '/has-admin-permissions',
        methods=['POST']
    )
    app.add_route(
        views.confirm_organization,
        '/confirm-organization',
        methods=['GET', 'POST']
    )
    app.add_route(
        views.configure_organization,
        '/configure-organization',
        methods=['GET', 'POST']
    )
    app.add_route(
        views.update_settings,
        '/update-settings',
        methods=['GET', 'POST']
    )
    app.add_route(
        views.AdminSetupView.as_view(),
        '/setup-admin-user',
        methods=['GET', 'POST']
    )
    app.add_route(
        views.ToolProfilesView.as_view(),
        '/tool-profiles',
        methods=['GET', 'POST']
    )
    app.add_route(
        views.ToolProfilesListView.as_view(),
        '/tool-profiles/list/',
        methods=['GET']
    )
    app.add_route(
        views.DeviceSetupView.as_view(),
        '/setup-device',
        methods=['GET', 'POST']
    )
    app.add_route(
        views.ReservationFormView.as_view(),
        '/tool-reservation/',
        methods=['GET', 'POST', 'OPTIONS']
    )
    app.add_route(
        views.ReservationListView.as_view(),
        '/tool-reservation/list/',
        methods=['GET', 'OPTIONS']
    )
    app.add_route(
        views.UserListView.as_view(),
        '/user/list/',
        methods=['GET']
    )
    app.add_route(
        views.UserDetailView.as_view(),
        '/user/',
        methods=['POST']
    )
    app.add_route(
        views.UserDetailView.as_view(),
        '/user/<id>/$',
        methods=['GET']
    )
    app.add_route(
        views.UpdateOutputStatusView.as_view(),
        '/update-output',
        methods=['POST']
    )
