import sys, os
proj_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, proj_path + '/../')

import asyncio
from signal import SIGINT, signal

from sanic import Sanic
from spf import SanicPluginsFramework
from sanic_cors import CORS
import socketio

from web import routes, views
from bus import bus

from web import feed_namespace

# sanic_cors and socketio cors conflict, disable socketio side
sio = socketio.AsyncServer(async_mode='sanic', cors_allowed_origins=[])
app = Sanic()
app.config['CORS_SUPPORTS_CREDENTIALS'] = True
spf = SanicPluginsFramework(app)

# highly permissible cors policy
cors = spf.register_plugin(
    CORS,
    resources={r"*": {"origins": "*"}}
)
routes.configure_routes(app)
sio.attach(app)
sio.register_namespace(feed_namespace.FeedNamespace('/feed'))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    server = app.create_server(
        host='0.0.0.0',
        port=8000,
        return_asyncio_server=True,
        asyncio_server_kwargs=None,
        debug=os.environ.get('DEBUG')
    )
    asyncio.ensure_future(server)
    # see https://github.com/ashleysommer/sanicpluginsframework/issues/12
    spf._on_server_start(app, loop) # force start

    signal(SIGINT, lambda s, f: loop.stop())
    try:
        loop.run_forever()
    except:
        loop.stop()
