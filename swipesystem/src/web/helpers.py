import json
import os
import re


DEVICE_CONFIG_FILE = (
    os.environ.get('DEVICE_CONFIG_FILE') or "/tmp/swipe-config.json"
)

DEVICE_CONFIGS_CACHE = None

def read_device_configs(clear_cache=False):
    global DEVICE_CONFIGS_CACHE
    if DEVICE_CONFIGS_CACHE and not clear_cache:
        return DEVICE_CONFIGS_CACHE

    device_configs = {}
    if os.path.exists(DEVICE_CONFIG_FILE):
        with open(DEVICE_CONFIG_FILE) as f:
            device_configs = json.load(f)
            print(device_configs)

    return device_configs


def write_device_configs(device_configs):
    with open(DEVICE_CONFIG_FILE, 'w') as f:
        f.write(json.dumps(device_configs))

    global DEVICE_CONFIGS_CACHE
    DEVICE_CONFIGS_CACHE = device_configs


OUTPUT_STATUS_FILE = (
    os.environ.get('OUTPUT_STATUS_FILE') or "/tmp/output-status.json"
)

OUTPUT_STATUS_CACHE = None


def read_output_status(clear_cache=False):
    global OUTPUT_STATUS_CACHE
    if OUTPUT_STATUS_CACHE and not clear_cache:
        return OUTPUT_STATUS_CACHE

    output_status = {}
    if os.path.exists(OUTPUT_STATUS_FILE):
        with open(OUTPUT_STATUS_FILE) as f:
            output_status = json.load(f)
            #print(output_status)

    return output_status


def write_output_status(output_status):
    with open(OUTPUT_STATUS_FILE, 'w') as f:
        f.write(json.dumps(output_status))

    global OUTPUT_STATUS_CACHE
    OUTPUT_STATUS_CACHE = output_status

    return output_status


MOCK_DEVICE = (bool(os.environ.get('MOCK_DEVICE')) or False)

def get_device_id():
        # REF: https://www.raspberrypi-spy.co.uk/2012/09/getting-your-raspberry-pi-serial-number-using-python/
        # Extract serial from cpuinfo file
        device_id = "0000000000000000"
        if MOCK_DEVICE == True:
            device_id = "MOCK0000000000"
        else:
            try:
                f = open('/proc/cpuinfo','r')
                for line in f:
                    if line[0:6]=='Serial':
                        device_id = line[10:26]
                f.close()
            except Exception as error:
                print(error)
                print("failed to get serial number")

        return device_id
