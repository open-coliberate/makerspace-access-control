import asyncio
import datetime as dt
import json
import logging
import os
from signal import SIGINT, signal
import subprocess

from sanic import Sanic
from sanic.response import file, json as sanic_json, text
from sanic.views import HTTPMethodView
import sqlalchemy

from web import helpers
import db


LOG = logging.getLogger(__file__)


async def status(request):
    return text('running')


async def get_device_info(request):
    device_id = helpers.get_device_id()

    session = db.get_session(db.engine)
    tool = session.query(db.Tool).filter_by(device_id=device_id).first()

    data = {
        'is_configured': True if tool else False,
        'device_id': device_id,
        'organization': helpers.read_device_configs().get('organization'),
        'timezone': helpers.read_device_configs().get('timezone')
    }
    return sanic_json(data)


async def user_has_email(request):
    card_uid = request.args.get('cardUid', '').rstrip()
    error = None
    user = None
    try:
        session = db.get_session(db.engine)
        user = session.query(db.User).filter_by(card_uid=card_uid).first()
    except Exception as e:
        error = str(e)

    data = {
        'status': 'success' if not error else error,
        'has_email': True if (user and user.email) else False,
        'user_exists': True if user else False,
    }
    return sanic_json(data)


async def configure_organization(request):
    """
    Updates organization url in device config file
    """
    if request.method == "GET":
        device_configs = helpers.read_device_configs()
        return sanic_json(device_configs)

    if request.method == "POST":
        device_configs = helpers.read_device_configs()
        device_configs['organization'] = (
            request.json.get('organization', device_configs.get('organization'))
        )
        device_configs['nickname'] = (
            request.json.get('nickname', device_configs.get('nickname'))
        )
        helpers.write_device_configs(device_configs)
        return sanic_json({"status": "success"})


async def confirm_organization(request):
    """
    Confirms short code through cloud webapp REST API
    """
    if request.method == "GET":
        return sanic_json(request)

    if request.method == "POST":
        error = None
        try:
            # TODO: connect to API
            pass
        except Exception as error:
            # TODO: log exception
            LOG.exception('Failed to confirm organization')
        return sanic_json({"status": "success" if not error else error})


class AdminSetupView(HTTPMethodView):
    def get(self, request):
        return sanic_json(request.json)

    def post(self, request):
        user, error = self.create_admin_user(
            request.json.get('email'),
            request.json.get('cardUid').rstrip()
        )
        return sanic_json({"status": "success" if not error else error})

    def create_admin_user(self, email : str, card_uid : str) -> (bool, list):
        """
        Returns (bool, list)
        """
        user, error = None, None
        try:
            session = db.get_session(db.engine)
            user = session.query(db.User).filter_by(email=email).first()
            if user:
                # TODO: only update card id if card id is not set?
                user.card_uid = card_uid
            else:
                user = db.User(email=email, card_uid=card_uid, is_admin=True)
            session.add(user)
            session.commit()
        except Exception as e:
            # TODO: log exception
            LOG.exception('Failed to create admin user')
            session.close()
            error = str(e)

        return user, error


class ToolProfilesListView(HTTPMethodView):
    def get(self, request):
        profiles, error = self.get_tool_profiles()
        return sanic_json({
            "status": "success" if not error else "failure",
            "data": profiles,
            "error": error
        })

    def get_tool_profiles(self):
      try:
          session = db.get_session(db.engine)
          profiles = session.query(db.ToolProfile).all()
          data, error = [], None
          for profile in profiles:
              data.append({
                "id": profile.id,
                "profileName": profile.profile_name,
                "powerOnSeconds" : profile.power_on_seconds,
                "warnAtSeconds" : profile.warn_at_seconds,
                "requiresTraining" : profile.requires_training,
                "restrictedToOpenHours" : profile.restricted_to_open_hours,
                "enableWarningSound" : profile.enable_warning_sound,
                "buddyMode" : profile.buddy_mode
              })
          return data, None
      except Exception as e:
          # TODO: log exception
          LOG.exception('Failed to get profiles')
          session.close()
          error = str(e)
      return None, error


class ToolProfilesView(HTTPMethodView):
    def get(self, request):
        device_id = request.args.get('deviceId') or helpers.get_device_id()
        session = db.get_session(db.engine)
        tool = session.query(db.Tool).filter_by(device_id=device_id).first()
        try:
            profile = session.query(db.ToolProfile).get(tool.tool_profile.id)
            return sanic_json({
                "status": "success",
                "data": self.get_tool_profile_as_json(profile)
            })
        except Exception as e:
            LOG.exception('Failed to retrieve tool profile')
            return sanic_json({
                "status": "failure",
                "error": str(e)
            })


    def post(self, request):
        tool_profile = self.create_or_update_tool_profile(
            request.json.get('id'),
            request.json.get('profileName'),
            request.json.get('powerOnSeconds'),
            request.json.get('warnAtSeconds'),
            request.json.get('requiresTraining'),
            request.json.get('restrictedToOpenHours'),
            request.json.get('enableWarningSound'),
            request.json.get('buddyMode')
        )
        return sanic_json({
            "status": "success" if tool_profile else "failure",
            "data": tool_profile.id if tool_profile else None
        })

    def get_tool_profiles(self):
      try:
          session = db.get_session(db.engine)
          profiles = session.query(db.ToolProfile).all()
          data = []
          for profile in profiles:
              item = self.get_tool_profile_as_json(profile)
              data.append(item)
          print(data)
          json_data = json.dumps(data)
          return json_data
      except Exception as e:
          # TODO: log exception
          LOG.exception('Failed to get profiles')
          session.close()
          error = str(e)
      return error

    def get_or_create_tool_profile(self, pk=None):
        """
        Pulls existing tool profile if one exists using its primary key (id)
        Otherwiwse returns a new ToolProfile object.
        """
        if pk:
            session = db.get_session(db.engine)
            return session.query(db.ToolProfile).get(pk)
        return db.ToolProfile()

    def create_or_update_tool_profile(
            self,
            id,
            profile_name,
            power_on_seconds : int,
            warn_at_seconds : int,
            requires_training,
            restricted_to_open_hours : bool,
            enable_warning_sound : bool,
            buddy_mode : bool
        ) -> db.ToolProfile:

        tool_profile = None
        session = db.get_session(db.engine)
        try:
            tool_profile = self.get_or_create_tool_profile(id)
            tool_profile.profile_name = profile_name
            tool_profile.power_on_seconds = power_on_seconds
            tool_profile.warn_at_seconds = warn_at_seconds
            tool_profile.requires_training = requires_training
            tool_profile.restricted_to_open_hours = restricted_to_open_hours
            tool_profile.enable_warning_sound = enable_warning_sound
            tool_profile.buddy_mode = buddy_mode

            session.add(tool_profile)
            session.commit()
        except Exception as e:
            LOG.exception('Failed to save tool profile: ')
            session.close()
        return tool_profile

    def get_tool_profile_as_json(self, profile):
        return {
            "id": profile.id,
            "profileName": profile.profile_name,
            "powerOnSeconds": profile.power_on_seconds,
            "warnAtSeconds": profile.warn_at_seconds,
            "requiresTraining": profile.requires_training,
            "restrictedToOpenHours": profile.restricted_to_open_hours,
            "enableWarningSound": profile.enable_warning_sound,
            "buddyMode": profile.buddy_mode
        }


class DeviceSetupView(HTTPMethodView):
    def get(self, request):
        return sanic_json(request.json)

    def post(self, request):
        # Switch to define based on profileName and ID? How to get ID?
        # Then add device ID from function asking pi or db, nickname blank or db
        device_id = helpers.get_device_id()
        tool = self.create_tool(
            request.json.get('toolProfileId'),
            device_id,
        )
        return sanic_json({"status": "success" if tool else "failure"})

    def create_tool(self, tool_profile_id, device_id):
        tool = None
        try:
            session = db.get_session(db.engine)
            tool = db.Tool(
                tool_profile_id=tool_profile_id,
                device_id=device_id
            )
            session.add(tool)
            session.commit()
        except Exception as e:
            LOG.exception('Failed to save tool')
            session.close()

        return tool


class DBMixin(object):
    def __init__(self):
        self._engine = None
        self._session = None

    @property
    def engine(self):
        if not self._engine:
            self._engine = db.engine
        return self._engine

    @property
    def session(self):
        if not self._session:
            self._session = db.get_session(self.engine)
        return self._session


class ReservationFormView(DBMixin, HTTPMethodView):
    def get(self, request):
        tool = self.get_device_tool()
        active_reservation = self.get_active_reservation(tool)

        resp = {}
        if active_reservation:
            resp = {
                'user': active_reservation.user.get_display_name(),
                'card_uid': active_reservation.user.card_uid,
                'duration_minutes': active_reservation.duration_seconds / 60.0,
                'start_time': active_reservation.start_time.isoformat(),
                'end_time': active_reservation.end_time.isoformat()
            }
        return sanic_json(resp)

    def post(self, request):
        action = request.json.get('action')
        card_uid = request.json.get('cardUid')
        card_uid = card_uid.replace('\n', '')
        handlers = {
            'create': self.handle_create,
            'extend': self.handle_extend,
            'cancel': self.handle_cancel
        }
        return handlers[action](card_uid)

    def handle_create(self, card_uid):
        tool = self.get_device_tool()
        if not tool:
            return sanic_json({
                'error': 'Device tool not properly configured. Check with admin'
            })

        user = self.session.query(db.User).filter_by(card_uid=card_uid).first()
        if not user:
            return sanic_json({
                'error': 'Unrecognized user'
            })

        active_reservation = self.get_active_reservation(tool)
        if active_reservation:
            return sanic_json({
                'error': 'Tool is currenlty in use'
            })
        # Note(Ahmed): if there exists two users with the same card, we only grab the 1st
        user = self.session.query(db.User).filter_by(card_uid=card_uid).first()
        reservation = db.ToolReservation(
            user=user,
            tool=tool,
            start_time=dt.datetime.now(),
            duration_seconds=tool.tool_profile.power_on_seconds or 30*60
        )
        self.session.add(reservation)
        self.session.commit()
        return sanic_json({'status': 'success'})

    def handle_extend(self, card_id):
        tool = self.get_device_tool()
        active_reservation = self.get_active_reservation(tool)
        active_reservation.extension_duration_seconds += tool.tool_profile.power_on_seconds
        self.session.add(active_reservation)
        self.session.commit()
        return sanic_json({'status': 'success'})

    def handle_cancel(self, card_id):
        tool = self.get_device_tool()
        active_reservation = self.get_active_reservation(tool)
        if not active_reservation:
            return sanic_json({'error': 'no active reservation to cancel'})
        active_reservation.is_cancelled = True
        active_reservation.cancellation_time = dt.datetime.now()
        self.session.add(active_reservation)
        self.session.commit()
        return sanic_json({'status': 'success'})

    def get_active_reservation(self, tool):
        past_reservations = (
            self.session
                .query(db.ToolReservation)
                .filter_by(tool=tool)
                .filter(db.ToolReservation.start_time <= dt.datetime.now())
                .all()  # TODO: optimize by getting the last one?
        )
        active_reservations = [
            r for r in past_reservations if
            r.end_time > dt.datetime.now() and r.is_cancelled != True
        ]
        return active_reservations[0] if active_reservations else None

    def get_device_tool(self):
        tool_uuid = helpers.read_device_configs().get('tool_uuid')
        tool = (
            self.session
                .query(db.Tool)
                .filter_by(uuid = tool_uuid)
                .first()
        )
        return tool

class ReservationListView(DBMixin, HTTPMethodView):
    def get(self, request):
        start = int(request.args.get('start', 0))
        size = int(request.args.get('size', 100))
        users = self.get_reservations_as_json(self.get_reservations(start, size))
        return sanic_json({
            'status': 'success',
            'reservations': users,
            'count': self.get_count()
        })

    def get_count(self):
        return self.session.query(db.ToolReservation).count()

    def get_reservations(self, start, size):
        return self.session.query(db.ToolReservation).offset(start).limit(size).all()

    def get_reservations_as_json(self, reservations):
        return [
            {
                'id': r.id,
                'user': r.user.email,
                'start_time': r.start_time.isoformat(),
                'end_time': r.end_time.isoformat(),
                'is_cancelled': r.is_cancelled,
                'cancellation_time': r.cancellation_time
            }
            for r in reservations
        ]


class UserListView(DBMixin, HTTPMethodView):
    def get(self, request):
        start = int(request.args.get('start', 0))
        size = int(request.args.get('size', 100))
        users = self.get_users_as_json(self.get_users(start, size))
        return sanic_json({
            'status': 'success',
            'users': users,
            'count': self.get_count()
        })

    def get_count(self):
        return self.session.query(db.User).count()

    def get_users(self, start, size):
        return self.session.query(db.User).offset(start).limit(size).all()

    def get_users_as_json(self, users):
        return [
            {
                'id': user.id,
                'email': user.email,
                'card_uid': user.card_uid,
                'is_admin': user.is_admin,
                'first_name': user.first_name,
                'last_name': user.last_name
            }
            for user in users
        ]


class UserDetailView(DBMixin, HTTPMethodView):
    def get(self, request, id):
        user = self.get_user_as_json(self.get_user(int(id)))
        return sanic_json({
            'status': 'success',
            'user': user
        })

    def post(self, request):
        try:
            if request.json.get('update', False):
                self.update(request.json)
            else:
                self.create(request.json)
        except Exception as e:
            print(e)
            self.session.flush()
            return sanic_json({'status': 'error', 'message': str(e)})
        return sanic_json({'status': 'success'})

    def update(self, form_data):
        card_uid = form_data.get('cardUid', '').rstrip()
        user = self.session.query(db.User).filter_by(card_uid=card_uid).first()

        update_data = {}
        # TODO: expand to allow for update of other fields
        if 'email' in form_data:
            update_data['email'] = form_data.get('email')
        if 'isAdmin' in form_data:
            update_data['is_admin'] = form_data.get('isAdmin')

        # Only update properties that have been passed
        for prop, value in update_data.items():
            setattr(user, prop, value)

        self.session.add(user)
        self.session.commit()

    def create(self, form_data):
        data = {
            'id': form_data.get('id'),
            'email': form_data.get('email'),
            'card_uid': form_data.get('cardUid', '').rstrip(),
            'is_admin': form_data.get('isAdmin')
        }

        # Create user obj with given form data
        user = db.User(**data)

        # Update existing user obj if it exists
        self.session.merge(user)

        # Persist user changes
        self.session.commit()

    def get_user(self, id):
        return self.session.query(db.User).get(id)

    def get_user_as_json(self, user):
        return {
            'id': user.id,
            'email': user.email,
            'card_uid': user.card_uid,
            'is_admin': user.is_admin,
            'first_name': '',
            'last_name': ''
        }


async def has_admin_permissions(request):
    card_uid = request.json.get('cardUid')
    session = db.get_session(db.engine)
    user = session.query(db.User).filter_by(card_uid=card_uid).first()
    is_admin = False
    if user and user.is_admin == True:
        is_admin = True
    return sanic_json({'result': is_admin})


class UpdateOutputStatusView(HTTPMethodView):
    def post(self, request):
        print(request.json.get('outputStatus'))
        switch_output = helpers.write_output_status({
            'outputStatus': request.json.get('outputStatus'),
        })
        return sanic_json({"status": "success" if switch_output else "failure"})


async def wifi_connect(request):
    rpi_wifi_connect_path = '/usr/src/app/wifi-connect'
    fake_wifi_connect_path = './wifi-connect'

    wifi_connect_path = (
        fake_wifi_connect_path
        if os.environ.get('MOCK_DEVICE')
        else rpi_wifi_connect_path
    )

    process = await asyncio.create_subprocess_shell(
        '{} --activity-timeout 180'.format(wifi_connect_path),
        stdout=asyncio.subprocess.PIPE
    )
    print('Started: wifi-connect process ' + '(pid = ' + str(process.pid) + ')')

       # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()

    if process.returncode == 0:
        print('Done:', '(pid = ' + str(process.pid) + ')')
    else:
        print('Failed:', '(pid = ' + str(process.pid) + ')')

    result = stdout.decode().strip()

    # Return stdout
    return sanic_json({
        'result': result
    })

def get_wifi_info(request):
    # Get the current SSID

    rpi_iwgetid_path = 'iwgetid'
    fake_iwgetid_path = './iwgetid'

    iwgetid_path = (
        fake_iwgetid_path
        if os.environ.get('MOCK_DEVICE')
        else rpi_iwgetid_path
    )

    SSID = None
    try:
        SSID = subprocess.check_output([iwgetid_path, "-r"]).strip()
    except subprocess.CalledProcessError:
        # If there is no connection subprocess throws a 'CalledProcessError'
        pass
    return sanic_json({
        'is_connected': True if SSID else False,
        'ssid': SSID
    })

async def update_settings(request):
    """
    Update device settings. Current supported settings,
    * timezone
    """

    if request.method == "GET":
        device_configs = helpers.read_device_configs()
        if not device_configs.get('timezone'):
            print('init default settings')
            device_configs.update({
                'timezone': 'America/New_York',
            })
            helpers.write_device_configs(device_configs)
        return sanic_json(device_configs)

    if request.method == "POST":
        device_configs = helpers.read_device_configs()
        device_configs['timezone'] = (
            request.json.get('timezone', device_configs.get('timezone', 'America/New_York'))
        )
        helpers.write_device_configs(device_configs)
        return sanic_json({"status": "success"})
