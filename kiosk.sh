# Run this script in display 0 - the monitor
export DISPLAY=:0

# Hide the mouse from the display
unclutter &

# If Chrome crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences

# Run Chromium and open tabs
startx /usr/bin/chromium-browser --disable-pinch --overscroll-history-navigation=0 \
    --kiosk --window-position=0,0 --no-sandbox --disable-infobars \
    --no-first-run --load-extension=/usr/src/app/virtual-keyboard \
    http://localhost:3000
