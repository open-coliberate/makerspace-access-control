# Intro developing on RPi

To get started with balenaOS checkout the [RaspberryPi3 Getting Started Page](https://www.balena.io/os/docs/raspberrypi3/getting-started/)

Read this document and stop at the [Running your first Container](https://www.balena.io/os/docs/raspberrypi3/getting-started/#Running-your-first-Container)

## Coliberate & Balena.io

Prebuilt files can be added to client-assets to speed up the build process on
the RPi. From the swipesystem-ui directory,

```
yarn run build
rm -r ../swipesystem/src/client-assets/*
cp -r build/* ../swipesystem/src/client-assets
```

Once you have flashed your raspberryPi3 with balenaOS according to the
instructions on the balenaOS getting started guide, you will then deploy the
Dockerfile in this repo to your raspberryPi. You will need the
[balena-cli](https://www.balena.io/docs/reference/cli/) for this. Balena
recently went through a name change from Resin. Some tools and resources may
still be marked under the Resin name.

You can deploy by running the following command,

```
$ git push balena <branch name>:master
```

You can use `--force` if writing over a different repository or branch.

Where balena.local is the local device hostname. This will build the
current Dockerfile on the RaspberryPI.

The Balena.io getting started document explains how to deploy dockerfiles to a
raspberryPi running BalenaOS with more clarity and is a useful supplement.


## Useful Balena-cli Commands in local mode

Scan for Balena devices,
```
$ sudo balena local scan
```

Build the current Dockerfile on the Raspberry Pi where `balena.local` is the
hostname found during the device scan.
```
$ sudo balena local push balena.local
```

To ssh directly into the selected container.
```
$ sudo balena local ssh balena.local
```

To connect to the host OS, we can add the --host option.

Stop the running container on Balena
```
$ sudo balena local stop
```

## Local development

To run the PI Display system locally ensure that you have installed,

1. sqlite3
2. Python 3
3. Python virtualenv
4. Run `bash start_local.sh`

Note: for python requirements installation use a virtual environment.
 * install `virtualenv` with `pip` if not installed already.
 * create virtual environment `$ virtualenv venv`
 * activate it `$ source venv/bin/activate`, you should now see (venv) in your
 curser line.
 * install requirements `$ pip install -r requirements.txt`, you may need to
 comment out `evdev` for use on OSX.

This will start the swipe system locally in two tmux sessions. One for the
frontend and one for the backend. To see the app open your browser navigate to
`localhost:3000`. This may take a moment to setup. The webapp is best viewed
at the display resolution 800x480.

(Tested on Arch and OSX.)

To simulate card scans you can use the `swipesystem/write_card_data.py` helper.

```
$ export EVDEV_READER_RESULTS_FILE=/tmp/cards.txt # set environment
$ python swipesystem/write_card_data.py 123 # write ID 123
```

You must tell `swipesystem/write_card_data.py` where the swipesystem is checking
for card read data.

You can use, `find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf` to
find and delete all `__pycache__`, `*.pyc`, and `*.pyo` files. 

#### Alternatively

If the local bash script did not work for you you can start the components
individually on your own.

* from the repository base move to `swipesystem`
* setup the environment variables

```
export $(grep -v '^#' sample.env | xargs)
```

* use `printenv` to check set variables

* from the repository base move to `swipesystem-ui`

```
cd pi_display/swipesystem-ui
```

* start react app,

```
yarn install
yarn start
```

* in another terminal window move to `swipesystem`
* setup the environment variables again like above
* move to `src`
* run `python3 db.py`
* run the background app (make sure python requirements are installed)

```
(venv) $ python main.py
Starting new swipesystsem
[2019-01-12 18:19:44 -0500] [48943] [INFO] Goin' Fast @ http://0.0.0.0:8000
```

* to clear existing testing DBs you can `rm /tmp/swipe-config.json` and
  `rm /tmp/swipe.db`.
* to recreate them `python3 swipesystem/src/db.py`
* and to explore them, `sqlite3 /tmp/swipe.db`
    * `.tables` to list tables, `select * from tool_profile;` to list all
      rows in said table.

#### Upon successful setup you will be greeted with the first device initiation screen

![Welcome Screen](swipesystem-ui/public/welcome_screen.png)
