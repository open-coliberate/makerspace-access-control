#!/bin/bash

## script starts 2 tmux sessions
# `tmux ls` to list sessions
# `tmux a -t name` to attach to session
# `ctrl-d` to exit
# `ctrl-b` then `d` to detach
# `tmux kill-server` to kill all tmux sessions

set -ex

# Remove database and config file if it exists
rm -f /tmp/{swipe-config.json,swipe.db,swipe-test.db,swipe-test-config.json}

setup () {
  # must have virtualenv installed
  # you may need to uncomment evdev from requirements on OSX
  if [ ! -e venv ] ; then
    virtualenv --python=python3.7 venv
    source venv/bin/activate
    pip install -r swipesystem/requirements.txt
  fi
  # setup environment and virtualenv
  export $(grep -v '^#' swipesystem/sample.env | xargs)
  echo "set environment variables"
  source venv/bin/activate
}

setup
python3 swipesystem/src/db.py

tmux new -s back -d
tmux send-keys -t back.0 "export $(grep -v '^#' swipesystem/sample.env | xargs)" ENTER
tmux send-keys -t back.0 "source venv/bin/activate" ENTER
tmux send-keys -t back.0 "cd swipesystem/src" ENTER
tmux send-keys -t back.0 "python main.py" ENTER

echo 'python server started at back'

tmux new -s front -d
tmux send-keys -t front.0 "cd swipesystem-ui" ENTER
tmux send-keys -t front.0 "yarn install" ENTER
tmux send-keys -t front.0 "yarn start" ENTER

echo 'yarn started at front'
