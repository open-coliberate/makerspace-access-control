import React from 'react';

import { Formik } from 'formik'
import { withStyles } from '@material-ui/core/styles';

import {
    FormControlLabel,
    FormGroup,
    Switch,
    TextField
} from '@material-ui/core'

import * as Yup from "yup";

const validationSchema = Yup.object().shape({
    profileName: Yup.string().required("Required field"),
    warnAtSeconds: Yup.number().integer().required("Required field"),
    powerOnSeconds: Yup.number().integer().required("Required field")
})

const styles = theme => ({
    textFieldD: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    root: {
        display: 'flex',
    },
    paper: {
        marginRight: theme.spacing.unit * 2,
    },
    formControl: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * -3,
        marginLeft: theme.spacing.unit * 5,
    },
    formControl2: {
        marginTop: theme.spacing.unit * -2,
        marginBottom: theme.spacing.unit * -3,
        marginLeft: theme.spacing.unit * 8,
    },
});


class ToolProfileForm extends React.Component {
    submitForm = () => {
        this._form.executeSubmit()
    }

    getForm = (classes, props) => {
        /**
         * Other fields accessible in props from formik that are not used:
         * (dirty, isSubmitting, handleSubmit, handleReset)
         */
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;

        return (
            <form className={classes.root}>
                <FormGroup component="fieldset" className={classes.formControl2}>
                    <TextField
                        id="profileName"
                        label="Tool Profile Name"
                        className={classes.textFieldD}
                        value={values.profileName}
                        error={touched.profileName && Boolean(errors.profileName)}
                        onBlur={handleBlur}
                        helperText={touched.profileName ? errors.profileName : ""}
                        onChange={handleChange}
                        margin="normal"
                    />
                    <TextField
                        id="powerOnSeconds"
                        label="Power on seconds"
                        type="number"
                        className={classes.textFieldD}
                        value={values.powerOnSeconds}
                        error={touched.powerOnSeconds && Boolean(errors.powerOnSeconds)}
                        onBlur={handleBlur}
                        helperText={touched.powerOnSeconds ? errors.powerOnSeconds : ""}
                        onChange={handleChange}
                        margin="normal"
                    />
                    <TextField
                        id="warnAtSeconds"
                        label="Warn on seconds"
                        type="number"
                        className={classes.textFieldD}
                        value={values.warnAtSeconds}
                        error={touched.warnAtSeconds && Boolean(errors.warnAtSeconds)}
                        onBlur={handleBlur}
                        helperText={touched.warnAtSeconds ? errors.warnAtSeconds : ""}
                        onChange={handleChange}
                        margin="normal"
                    />
                </FormGroup>

                <FormGroup component="fieldset" className={classes.formControl}>
                    <FormControlLabel
                        control={
                            <Switch
                                id="requiresTraining"
                                checked={values.requiresTraining}
                                value="requiresTraining"
                                onChange={handleChange('requiresTraining')}
                                color="primary"
                            />
                        }
                        label="Tool requires training?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                id="restrictedToOpenHours"
                                checked={values.restrictedToOpenHours}
                                value="restrictedToOpenHours"
                                onChange={handleChange('restrictedToOpenHours')}
                                color="primary"
                            />
                        }
                        label="Restricted by space hours?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                id="enableWarningSound"
                                checked={values.enableWarningSound}
                                value="enableWarningSound"
                                onChange={handleChange('enableWarningSound')}
                                color="primary"
                            />
                        }
                        label="Enable warning sound?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                id="buddyMode"
                                checked={values.buddyMode}
                                value="buddyMode"
                                onChange={handleChange('buddyMode')}
                                color="primary"
                            />
                        }
                        label="Enable buddy mode?"
                    />
                </FormGroup>
            </form>
        )
    }
    render() {
        const { classes } = this.props;
        return (
                <Formik
                    enableReinitialize
                    initialValues={this.props.initialValues}
                    validationSchema={validationSchema}
                    onSubmit={this.props.handleSubmit.bind(this)}
                    ref={node => this._form = node}
                    >
                    {props => this.getForm(classes, props)}
                </Formik>
        )
    }
}

export default withStyles(styles)(ToolProfileForm);