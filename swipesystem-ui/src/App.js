import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import SetupForm from './views/SetupForm';
import Dashboard from './views/Dashboard'
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import { Refresh } from 'mdi-material-ui'

const styles = theme => ({
  root: {
    display: 'flex',
    textAlign: 'center',
  },
  loading: {
    display: 'flex',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  },
  refresh: {
    position: 'absolute',
    right: '50px',
    bottom: '50px',
  },
  progress: {
    margin: theme.spacing.unit * 2,
  }
});

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      is_configured: false,
      organization: null,
      machine_id: null,
      error: null,
      isLoading: false
    }
    this.changeConfiguredState = this.changeConfiguredState.bind(this);
  }


  // change state of is_configured from child
  changeConfiguredState = (value) => {
   this.setState({
    is_configured: value
   });
  }


  checkState = () => {
    fetch('http://localhost:8000/device-info')
      .then(response => response.json())
      .then(data => this.setState({...data,
                                   isLoading: false}))
      .catch(err => this.setState({ error: err }))
  };


  componentWillMount() {
    this.setState({ isLoading: true })
    this.checkState();
  }


  render() {
    // TODO: set up error view

    if (this.state.isLoading) {
      return (
        <div className={this.props.classes.loading}>
          <CircularProgress className={this.props.classes.progress} />
          Initializing device...
          <div className={this.props.classes.refresh}>
            <Fab
              color="primary"
              onClick={this.checkState}
              className={this.props.classes.fab}
            >
              <Refresh />
            </Fab>
          </div>
        </div>
      )
    }

    return (
      <div className={this.props.classes.root}>
        {this.state.is_configured ? <Dashboard /> :
          <SetupForm changeConfiguredState={this.changeConfiguredState}/>}
      </div>
    );
  }
}

export default withStyles(styles)(App);
