import { connect } from 'react-redux'
import * as actions from '../actions/settings.js'

import SettingsDashboard from '../views/Dashboard/SettingsDashboard'


export default connect(
    (state) => ({
        settings: state.settings
    }),
    {
        updateTimezone: actions.updateTimezone,
    }
)(SettingsDashboard)
