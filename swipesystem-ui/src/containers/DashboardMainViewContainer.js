import { connect } from 'react-redux'
import * as actions from '../actions/settings.js'

import DashboardMainView from '../views/Dashboard/DashboardMainView'


export default connect(
    (state) => ({
        settings: state.settings
    }),
    {
        updateTimezone: actions.updateTimezone,
    }
)(DashboardMainView)
