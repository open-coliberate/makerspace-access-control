import React from 'react'

import { Formik } from 'formik'
import { withStyles } from '@material-ui/core/styles';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';

import * as Yup from "yup";

const ITEM_HEIGHT = 48;

const styles = theme => ({
    textFieldD: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    root: {
        display: 'flex',
    },
    paper: {
      marginRight: theme.spacing.unit * 2,
    },
    formControl: {
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * -3,
      marginLeft: theme.spacing.unit * 5,
    },
    formControl2: {
      marginTop: theme.spacing.unit * -2,
      marginBottom: theme.spacing.unit * -3,
      marginLeft: theme.spacing.unit * 8,
    },
});

const validationSchema = Yup.object().shape({
    profileName: Yup.string().required("Required field"),
    warnAtSeconds: Yup.number().integer().required("Required field"),
    powerOnSeconds: Yup.number().integer().required("Required field")
})

class DeviceForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            initialValues: {
                profileName: "",
                powerOnSeconds: "",
                warnAtSeconds: "",
                requiresTraining: false,
                restrictedToOpenHours: false,
                enableWarningSound: false,
                buddyMode: false,
            },
            deviceId: "",
            newProfile: false,
            open: false,
            profiles: [],
            message: "",
        };
    }

    handleChange = name => event => {
        this.setState(state => ({ [name]: event.target.checked }));
    };

    handleToggle = () => {
      /* if closed and toggling open, get existing profiles */
      if (this.state.open === false ) {
        this.getExistingProfiles();
        console.log(this.state.profiles);
      }
      this.setState(state => ({ open: !state.open }));
    };

    handleClose = event => {
      if (this.anchorEl.contains(event.target)) {
        return;
      }
      this.setState({ open: false });
    };

    handleNewProfile = () => {
        this.resetInitialValues()
        this.setState({newProfile: true})
    }

    async handleSelectProfile(data) {
        this.setState({newProfile: false})
        console.log("data")
        console.log(data)
        console.log(data.profileName)
        var selectedProfile = this.state.profiles.filter(function (chain) {
            return chain.profileName === data.profileName;
        })[0];
        console.log(selectedProfile)
        this.setState({initialValues: selectedProfile})
    };

    handleNoProfile = () => {
        this.setState({message: "Please Select Profile"})
    };

    getInitialValues () {
        return this.state.initialValues;
    }

    resetInitialValues () {
        this.setState({
          initialValues: {
              profileName: "",
              powerOnSeconds: "",
              warnAtSeconds: "",
              requiresTraining: false,
              restrictedToOpenHours: false,
              enableWarningSound: false,
              buddyMode: false,
          }
        })
    }

    getMessage() {
        return this.state.message;
    }

    executeSubmit() {
        this._form.submitForm();
    }

    updateNewProfile(newProfile) {
        this.setState({newProfile: newProfile})
    }

    async getExistingProfiles() {
        try {
            const resp = await fetch('http://localhost:8000/tool-profiles/list/')
            let respJson = await resp.json();
            console.log(respJson.data);
            this.setState({profiles: respJson.data})
            console.log(this.state.profiles)
            return respJson;
        } catch (error) {
            console.log('failed to get tool profiles', error)
        }
    }

    async handleSubmitProfile(data) {
        try {
            const resp = await fetch("http://localhost:8000/tool-profiles", {
                method: "POST",
                body: JSON.stringify(data)
            })
            const result = await resp.json()
            console.log(result)
            console.log('done')
            return result.data
            // this.props.handleComplete()
        } catch (error) {
            console.log('failed to submit', error)
        }
    }

    async handleSubmit(data) {
        let toolProfileId = null;
        try {
            if (this.state.newProfile) {
                toolProfileId = await this.handleSubmitProfile(data)
            }
            const resp = await fetch("http://localhost:8000/setup-device", {
                method: "POST",
                body: JSON.stringify({...data, toolProfileId})
            })
            console.log(await resp.json())
            console.log('done')
            this.props.handleComplete()
        } catch (error) {
            console.log('failed to submit', error)
        }
    }

    getProfileForm(classes, props) {
        /**
         * Other fields accessible in props from formik that are not used:
         * (dirty, isSubmitting, handleSubmit, handleReset)
         */
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;

        return (
            <form className={classes.root}>
                <FormGroup component="fieldset" className={classes.formControl2}>
                    <TextField
                        id="profileName"
                        label="Tool Profile Name"
                        className={classes.textFieldD}
                        value={values.profileName}
                        error={touched.profileName && Boolean(errors.profileName)}
                        onBlur={handleBlur}
                        helperText={touched.profileName ? errors.profileName : ""}
                        onChange={handleChange}
                        margin="normal"
                    />
                    <TextField
                        id="powerOnSeconds"
                        label="Power on seconds"
                        type="number"
                        className={classes.textFieldD}
                        value={values.powerOnSeconds}
                        error={touched.powerOnSeconds && Boolean(errors.powerOnSeconds)}
                        onBlur={handleBlur}
                        helperText={touched.powerOnSeconds ? errors.powerOnSeconds : ""}
                        onChange={handleChange}
                        margin="normal"
                    />
                    <TextField
                        id="warnAtSeconds"
                        label="Warn on seconds"
                        type="number"
                        className={classes.textFieldD}
                        value={values.warnAtSeconds}
                        error={touched.warnAtSeconds && Boolean(errors.warnAtSeconds)}
                        onBlur={handleBlur}
                        helperText={touched.warnAtSeconds ? errors.warnAtSeconds : ""}
                        onChange={handleChange}
                        margin="normal"
                    />
                </FormGroup>

                <FormGroup component="fieldset" className={classes.formControl}>
                    <FormControlLabel
                        control={
                            <Switch
                                id="requiresTraining"
                                checked={this.state.requiresTraining}
                                value="requiresTraining"
                                onChange={handleChange('requiresTraining')}
                                color="primary"
                            />
                        }
                        label="Tool requires training?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                id="restrictedToOpenHours"
                                checked={this.state.restrictedToOpenHours}
                                value="restrictedToOpenHours"
                                onChange={handleChange('restrictedToOpenHours')}
                                color="primary"
                            />
                        }
                        label="Restricted by space hours?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                id="enableWarningSound"
                                checked={this.state.enableWarningSound}
                                value="enableWarningSound"
                                onChange={handleChange('enableWarningSound')}
                                color="primary"
                            />
                        }
                        label="Enable warning sound?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                id="buddyMode"
                                checked={this.state.buddyMode}
                                value="buddyMode"
                                onChange={handleChange('buddyMode')}
                                color="primary"
                            />
                        }
                        label="Enable buddy mode?"
                    />
                </FormGroup>
            </form>
        )
    }

    getDisabledProfileForm(classes, props) {
        /**
         * Other fields accessible in props from formik that are not used:
         * (dirty, isSubmitting, handleSubmit, handleReset)
         */
        const {
            values,
            touched,
            errors,
            handleBlur,
        } = props;

        return (
            <form className={classes.root}>
                <FormGroup component="fieldset" className={classes.formControl2}>
                    <TextField
                        disabled
                        id="profileName"
                        label="Tool Profile Name"
                        className={classes.textFieldD}
                        value={values.profileName}
                        error={touched.profileName && Boolean(errors.profileName)}
                        onBlur={handleBlur}
                        helperText={touched.profileName ? errors.profileName : ""}
                        margin="normal"
                    />
                    <TextField
                        disabled
                        id="powerOnSeconds"
                        label="Power on seconds"
                        type="number"
                        className={classes.textFieldD}
                        value={values.powerOnSeconds}
                        error={touched.powerOnSeconds && Boolean(errors.powerOnSeconds)}
                        onBlur={handleBlur}
                        helperText={touched.powerOnSeconds ? errors.powerOnSeconds : ""}
                        margin="normal"
                    />
                    <TextField
                        disabled
                        id="warnAtSeconds"
                        label="Warn on seconds"
                        type="number"
                        className={classes.textFieldD}
                        value={values.warnAtSeconds}
                        error={touched.warnAtSeconds && Boolean(errors.warnAtSeconds)}
                        onBlur={handleBlur}
                        helperText={touched.warnAtSeconds ? errors.warnAtSeconds : ""}
                        margin="normal"
                    />
                </FormGroup>

                <FormGroup component="fieldset" className={classes.formControl}>
                    <FormControlLabel
                        control={
                            <Switch
                                disabled
                                id="requiresTraining"
                                checked={this.state.initialValues.requiresTraining}
                                value={this.state.initialValues.requiresTraining}
                                color="primary"
                            />
                        }
                        label="Tool requires training?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                disabled
                                id="restrictedToOpenHours"
                                checked={this.state.initialValues.restrictedToOpenHours}
                                value={this.state.initialValues.restrictedToOpenHours}
                                color="primary"
                            />
                        }
                        label="Restricted by space hours?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                disabled
                                id="enableWarningSound"
                                checked={this.state.initialValues.enableWarningSound}
                                value={this.state.initialValues.enableWarningSound}
                                color="primary"
                            />
                        }
                        label="Enable warning sound?"
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                disabled
                                id="buddyMode"
                                checked={this.state.initialValues.buddyMode}
                                value={this.state.initialValues.buddyMode}
                                color="primary"
                            />
                        }
                        label="Enable buddy mode?"
                    />
                </FormGroup>
            </form>
        )
    }

    render() {
        const { classes } = this.props;
        const { open } = this.state;
        const isComplete = this.props.isComplete

        if (isComplete) {
            return (<div></div>)
        } else if (this.props.nickname) {
            return (<div></div>)
        } else {
            return (
                <React.Fragment>
                    <Typography variant="subtitle1" gutterBottom>
                        <div>Assign a Tool Profile to this Device</div>
                    </Typography>
                    <Typography variant="subtitle1" color="error">
                        {/*error: no profile is selected!*/}
                        <div>{this.getMessage()}</div>
                    </Typography>
                    {/* Menu to select or create tool profile */}
                    <div>
                      <Button
                        variant="outlined"
                        aria-label="More"
                        aria-owns={open ? 'long-menu' : undefined}
                        aria-haspopup="true"
                        buttonRef={node => {
                          this.anchorEl = node;
                        }}
                        onClick={this.handleToggle}
                      >
                        Select Tool Profile
                      </Button>
                      <Menu
                        id="long-menu"
                        anchorEl={this.anchorEl}
                        anchorOrigin={{
                          vertical: 'top',
                          horizontal: 'center',
                        }}
                        transformOrigin={{
                          vertical: 'top',
                          horizontal: 'center',
                        }}
                        open={open}
                        onClose={this.handleToggle}
                        PaperProps={{
                          style: {
                            maxHeight: ITEM_HEIGHT * 4.5,
                          },
                        }}
                      >
                        <MenuItem onClick={this.handleNewProfile}>
                          Create New Profile
                        </MenuItem>
                        {this.state.profiles.map((profile, index) => (
                          <MenuItem
                            key={index}
                            onClick={() => this.handleSelectProfile(profile)}
                            value={profile.profileName}
                          >
                            {profile.profileName}
                          </MenuItem>
                        ))}
                      </Menu>
                    </div>
                    {/* If newProfile is true then show this form */}
                    {this.state.newProfile &&
                        <Formik
                            initialValues={this.getInitialValues()}
                            validationSchema={validationSchema}
                            onSubmit={this.handleSubmit.bind(this)}
                            ref={node => this._form = node}
                        >
                            {props => {
                                return (
                                    this.getProfileForm(classes, props)
                                )
                            }}
                        </Formik>
                    }
                    {/* If Profile is selected then show this form */}
                    {this.state.initialValues.profileName.length > 0 &&
                        <Formik
                            initialValues={this.getInitialValues()}
                            validationSchema={validationSchema}
                            onSubmit={this.handleSubmit.bind(this)}
                            ref={node => this._form = node}
                        >
                            {props => {
                                return (
                                    this.getDisabledProfileForm(classes, props)
                                )
                            }}
                        </Formik>
                    }
                    {/* Blank form */}
                    {this.state.initialValues.profileName.length === 0 &&
                        !this.state.newProfile &&
                        <Formik
                            onSubmit={this.handleNoProfile}
                            ref={node => this._form = node}
                        >
                        </Formik>
                    }
                </React.Fragment>
            )
        }
    }
}

export default withStyles(styles)(DeviceForm);
