import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Stepper from '@material-ui/core/Stepper';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import { withStyles } from '@material-ui/core/styles';

import OrganizationForm from './OrganizationForm'
import DeviceForm from './DeviceForm'
import AdminSetupForm from './AdminSetupForm'

import { Redirect } from 'react-router-dom'


const grn = createMuiTheme({
  palette: {
    primary: { main: green[500] },
  },
  typography: { useNextVariants: true },
});


const styles = theme => ({
  setupRoot: {
    display: 'flex',
    alignSelf: 'flex-start',
    justifyContent: 'center',
    width: '90%',
    marginLeft: '3%',
    height: '100vh'
  },
  setupContent: {
    marginTop: '20px',
    alignSelf: 'flex-start',
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  stepControls: {
    marginTop: 40
  }
});


function getSteps() {
  return [
    'Setup admin user (required)',
    'Setup organization (optional)',
    'Setup this device (optional)'];
}


class SetupForm extends React.Component {
  constructor(props) {
    super(props)
    this.forms = [null, null, null]

    this.state = {
      activeStep: 0,
      completed: {},
      codeVerify: false,
    }
    this.changeCodeVerifyState = this.changeCodeVerifyState.bind(this);
  }


  // change state of codeVerify from child
  changeCodeVerifyState = (value) => {
    this.setState({
      codeVerify: value
    });
  }


  // change state of is_configured in parent
  changeConfiguredState = (value) => {
    console.log(value)
    this.props.changeConfiguredState(value);
  }


  totalSteps = () => {
    return getSteps().length;
  };


  handleNext = () => {
    let activeStep;

    activeStep = this.state.activeStep + 1;

    // completed step skip forward
    if (this.state.completed[this.state.activeStep]) {
      this.handleSkip();
    }
    // final step submit and finish configuration
    else if (activeStep >= this.totalSteps()) {
      this.handleSubmit();
      //this.changeConfiguredState(true)
      //console.log(this.state.is_configured)
    }
    // other unfinished steps
    else {
      this.handleSubmit();
    }
  };


  handleSkip = () => {
    let activeStep;

    activeStep = this.state.activeStep + 1;

    if (activeStep >= this.totalSteps()) {
      // set is_configured status to true
      this.changeConfiguredState(true)
    } else {
      this.setState({
        activeStep,
      });
    }
  };


  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleStep = step => () => {
    this.setState({
      activeStep: step,
    });
  };

  handleSubmit = () => {
    console.log(this.state)
    const currentForm = this.forms[this.state.activeStep];
    console.log(this, currentForm.executeSubmit())
  }


  handleComplete = () => {
    const { completed } = this.state;
    completed[this.state.activeStep] = true;
    this.setState({
      completed,
    });
    this.handleSkip();
    // this.handleBack()
  };


  handleReset = () => {
    this.setState({
      activeStep: 0,
      completed: {},
    });
  };

  completedSteps() {
    return Object.keys(this.state.completed).length;
  }

  isLastStep() {
    return this.state.activeStep === this.totalSteps() - 1;
  }

  allStepsCompleted() {
    return this.completedSteps() === this.totalSteps();
  }

  requiredStepsCompleted() {
    return this.completedSteps() === this.totalSteps();
  }


  getStepContent(step) {
    switch (step) {
      case 0:
        return <AdminSetupForm
                  innerRef={node => this.forms[step] = node}
                  handleComplete={this.handleComplete}
                  isComplete={this.state.completed[this.state.activeStep]}
                />;
      case 1:
        return <OrganizationForm
                  innerRef={node => this.forms[step] = node}
                  handleComplete={this.handleComplete}
                  isComplete={this.state.completed[this.state.activeStep]}
                  codeVerify={this.state.codeVerify}
                  changeCodeVerifyState={this.changeCodeVerifyState}
                />
      case 2:
        return <DeviceForm
                  innerRef={node => this.forms[step] = node}
                  handleComplete={this.handleComplete}
                  isComplete={this.state.completed[this.state.activeStep]}
                />
      default:
        return 'Unknown step';
    }
  }

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.setupRoot}>
        <div className={classes.setupContent}>
          <Stepper nonLinear activeStep={activeStep}>
            {steps.map((label, index) => {
              return (
                <Step key={label}>
                  {/* move between steps with top bar unless on 1st required
                    page when it is incomplete */}
                  <StepButton
                    disabled={activeStep === 0 &&
                      !this.state.completed[this.state.activeStep]}
                    onClick={this.handleStep(index)}
                    completed={this.state.completed[index]}
                  >
                    {label}
                  </StepButton>
                </Step>
              );
            })}
          </Stepper>
          <div>
            {this.requiredStepsCompleted() ? (
              <div>
                {/* <Typography className={classes.instructions}>
                  All steps completed - you&apos;re finished
                </Typography>
                <Button onClick={this.handleReset}>Reset</Button> */}
                <Redirect to="/dashboard" />
              </div>
            ) : (
                <div>
                  {this.getStepContent(activeStep)}

                  <div className={classes.stepControls}>
                    {/* Back on all screens, disabled on first */}
                    <Button
                      disabled={activeStep === 0}
                      onClick={this.handleBack}
                      className={classes.button}
                    >
                      Back
                    </Button>
                    {/* Next on all screens, Finish if all other steps are
                      complete and on uncomplete page */}
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleNext}
                      className={classes.button}
                    >
                      {(this.completedSteps() === this.totalSteps() - 1) &&
                        (!this.state.completed[this.state.activeStep]) ?
                        'Finish' : 'Next'}
                    </Button>
                    {/* Skip if on the second to last screen unless asking for
                      short code */}
                    {(activeStep === this.totalSteps() - 2) &&
                      (!this.state.codeVerify) ? (
                      <Button
                        variant="contained"
                        color="default"
                        onClick={this.handleSkip}
                        className={classes.button}
                      >
                        Skip
                      </Button>
                      /* Exit Setup incomplete as option if on last page and
                        all other steps are complete */
                    ) : this.isLastStep() && !this.allStepsCompleted() ? (
                      <Button
                        variant="contained"
                        color="default"
                        onClick={this.handleSkip}
                        className={classes.button}
                      >
                        Exit Setup
                      </Button>
                      /* If no other conditions then no third button */
                    ) : (
                      null
                    )}
                    {/* If the step is complete show a green check box */}
                    {this.state.completed[this.state.activeStep] ? (
                          <Typography
                            variant="caption"
                            className={classes.completed}
                          >
                              <MuiThemeProvider theme={grn}>
                                  <Checkbox checked={true} color="primary"/>
                              </MuiThemeProvider>
                              Step {activeStep + 1} complete
                          </Typography>
                      ) : (
                          null
                        )}
                  </div>
                </div>
              )}
          </div>
        </div>
      </div>
    );
  }
}

SetupForm.propTypes = {
  classes: PropTypes.object,
};


export default withStyles(styles)(SetupForm);
