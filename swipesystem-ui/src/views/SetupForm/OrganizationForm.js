import React from 'react'

import { Formik } from 'formik'
import { withStyles } from '@material-ui/core/styles';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';

import * as Yup from "yup";

const ITEM_HEIGHT = 48;


const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 400,
    },
    marginTop: {
      marginTop: theme.spacing.unit,
    },
});


const validationSchema = Yup.object().shape({
    organization: Yup.string().required("Required field"),
    code: Yup.number().integer(),
})


class OrganizationForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            initialValues: {
              organization:"",
              nickname:"",
              code:"",
              selectedHub:"",
            },
            open: false,
            hubs: [],
            message: "",
        };
    }


    // change state of codeVerify in parent
    changeCodeVerifyState = (value) => {
      console.log(value)
      this.props.changeCodeVerifyState(value);
    }


    executeSubmit() {
        this._form.submitForm();
    }


    getInitialValues () {
        return this.state.initialValues;
    }


    async handleSubmit(data) {
        try {
            this.handleSubmitCode(data)
            const resp = await fetch("http://localhost:8000/configure-organization", {
                method: "POST",
                body: JSON.stringify(data)
            })
            console.log(await resp.json())
            console.log('done')
            this.props.handleComplete()
        } catch (error) {
            console.log('failed to submit', error)
        }
    }


    handleToggle = () => {
      /* if closed and toggling open, get existing profiles */
      if (this.state.open === false ) {
        console.log(this.state.hubs);
      }
      this.setState(state => ({ open: !state.open }));
    };


    handleClose = event => {
      if (this.anchorEl.contains(event.target)) {
        return;
      }
      this.setState({ open: false });
    };


    async handleSelectHub(data) {
        console.log("data")
        console.log(data)
        console.log(data.title)
        var selectedHub = this.state.hubs.filter(function (chain) {
            return chain.title === data.title;
        })[0];
        console.log(selectedHub)
        this.setState({initialValues: selectedHub})
    };


    async handleSubmitCode(data) {
        try {
            const resp = await fetch("http://localhost:8000/confirm-organization", {
                method: "POST",
                body: JSON.stringify(data)
            })
            console.log(await resp.json())
            console.log('done')
        } catch (error) {
            console.log('failed to verify code', error)
        }
    }


    async handleGetCode(data) {
        try {
            this.changeCodeVerifyState(true)
        } catch (error) {
            console.log('failed to change form', error)
        }
    }


    getForm(classes, props) {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;
        return (
            <form>
                <TextField
                    id="organization"
                    label="Organization"
                    className={classes.textField}
                    value={values.organization}
                    error={touched.organization && Boolean(errors.organization)}
                    onBlur={handleBlur}
                    helperText={touched.organization ? errors.organization : ""}
                    onChange={handleChange}
                    margin="normal"
                    InputProps={{
                        endAdornment: (
                            <InputAdornment variant="filled" position="end">
                                .coliberate.io
                            </InputAdornment>
                        ),
                    }}
                />
                <TextField
                    id="nickname"
                    label="Device Nickname"
                    error={touched.nickname && Boolean(errors.nickname)}
                    onBlur={handleBlur}
                    helperText={touched.nickname ? errors.nickname : ""}
                    className={classes.textField}
                    value={values.nickname}
                    onChange={handleChange}
                    margin="normal"
                />
            </form>
        )
    }


    getCodeForm(classes, props) {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;
        return (
            <form
            >
                <TextField
                    id="code"
                    label="Short Code"
                    error={touched.code && Boolean(errors.code)}
                    onBlur={handleBlur}
                    helperText={touched.code ? errors.code : ""}
                    className={classes.textField}
                    value={values.code}
                    onChange={handleChange}
                    margin="normal"
                />
            </form>
        )
    }


    render() {
        const { classes } = this.props;
        const { open } = this.state;
        const isComplete = this.props.isComplete

        if (isComplete) {
            return (<div></div>)
        }
        else if (this.props.codeVerify) {
            /* Verify code, save organization if valid */
            return (
              <React.Fragment>
                  <Typography variant="subtitle1">
                      <div>Check your Admin Email for a Short Code to Input Here</div>
                  </Typography>
                  <Formik
                      initialValues={this.getInitialValues()}
                      validationSchema={validationSchema}
                      onSubmit={this.handleSubmit.bind(this)}
                      ref={node => this._form = node}
                  >
                      {props => {
                          return (
                              this.getCodeForm(classes, props)
                          )
                      }}
                  </Formik>
                  {/* Menu to select your hub */}
                  <div>
                    <Button
                      variant="outlined"
                      aria-label="More"
                      aria-owns={open ? 'long-menu' : undefined}
                      aria-haspopup="true"
                      buttonRef={node => {
                        this.anchorEl = node;
                      }}
                      onClick={this.handleToggle}
                      className={classes.marginTop}
                    >
                      Select Your Hub
                    </Button>
                    <Menu
                      id="long-menu"
                      anchorEl={this.anchorEl}
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                      open={open}
                      onClose={this.handleToggle}
                      PaperProps={{
                        style: {
                          maxHeight: ITEM_HEIGHT * 4.5,
                        },
                      }}
                    >
                      {/* If list of hubs is empty */}
                      {this.state.hubs.length === 0 &&
                        <MenuItem onClick={this.handleClose}>
                          No Hubs, Create One On CoLiberate.io!
                        </MenuItem>
                      }
                      {/* List of hubs */}
                      {this.state.hubs.map((hub, index) => (
                        <MenuItem
                          key={index}
                          onClick={() => this.handleSelectHub(hub)}
                          value={hub.title}
                        >
                          {hub.title}
                        </MenuItem>
                      ))}
                    </Menu>
                  </div>
              </React.Fragment>
            )
        } else {
            /* Get short code, change form */
            return (
                <React.Fragment>
                    <Typography variant="subtitle1">
                        <div>Attach this Device to an Organization Here</div>
                    </Typography>
                    <Formik
                        initialValues={this.getInitialValues()}
                        validationSchema={validationSchema}
                        onSubmit={this.handleGetCode.bind(this)}
                        ref={node => this._form = node}
                    >
                        {props => {
                            return (
                                this.getForm(classes, props)
                            )
                        }}
                    </Formik>
                </React.Fragment>
            )
        }
    }
}

export default withStyles(styles)(OrganizationForm);
