import React from 'react'

import classNames from 'classnames';
import { Formik } from 'formik'
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import * as Yup from "yup";

import io from 'socket.io-client'

const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 400,
    },
    margin: {
        margin: theme.spacing.unit,
    }
});

const validationSchema = Yup.object().shape({
    email: Yup.string().email().required("Required field"),
})


class DeviceForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            cardUid: "",
            initialValues: {email: ''},
            message: "",
        };
    }


    updateCardUid(cardUid) {
        this.setState({cardUid: cardUid})
    }

    componentDidMount() {
        console.log('Opening socket')
        this.socket = io('http://localhost:8000/feed')
        this.socket.on('data', (event) => {
            if (event.type === 'card-swipe') {
                this.updateCardUid(event.payload.card_id)
            }
        })
    }

    componentWillUnmount() {
        console.log('Closing socket')
        this.socket.close();
    }

    executeSubmit() {
        this._form.submitForm();
    }

    getInitialValues() {
        return this.state.initialValues;
    }


    getMessage() {
        return this.state.message;
    }


    async handleSubmit(data) {
        data = {...data, cardUid: this.state.cardUid}
        try {
            const resp = await fetch("http://localhost:8000/setup-admin-user", {
                method: "POST",
                body: JSON.stringify(data)
            })
            var response = await resp.json()
            console.log()
            if (response["status"] === "success") {
                console.log('done')
                this.props.handleComplete()
            } else {
                throw response["status"];
            }
        } catch(error) {
            var err
            console.log(error)
            if (error.includes("CHECK constraint failed")){
                err = "Missing Card ID";
            } else if (error.includes("UNIQUE constraint failed")){
                err = "Card ID already in use";
            }
            this.setState({message: err})
            console.log('failed to submit', err);
        }
    }


    getForm(classes, props) {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;
        return (
            <form>
                <TextField
                    id="email"
                    className={classes.textField}
                    error={touched.email && Boolean(errors.email)}
                    onBlur={handleBlur}
                    label="email"
                    helperText={touched.email ? errors.email : ""}
                    value={values.email}
                    onChange={handleChange}
                    margin="normal"
                />
                <TextField
                    disabled
                    id="cardUid"
                    className={classNames(classes.margin, classes.textField)}
                    error={errors.cardUid}
                    type='password'
                    label="Card ID"
                    helperText="Swipe your card on the card reader"
                    value={this.state.cardUid}
                />
            </form>
        )
    }

    render() {
        const { classes } = this.props;
        const isComplete = this.props.isComplete

        if (isComplete) {
            return (<div></div>)
        } else {
            return (
                <React.Fragment>
                    <Typography variant="h5">
                        <div>Welcome to Swipe!</div>
                    </Typography>
                    <Typography variant="subtitle1">
                        <div>Please Configure an Admin User Here</div>
                    </Typography>
                    <Typography variant="subtitle1" color="primary">
                        {/*tip: register this user as a CoLiberate webapp admin or use kiosk in local mode.*/}
                        <div></div>
                    </Typography>
                    <Typography variant="subtitle1" color="error">
                        {/*error: admin user not registered on the webapp!*/}
                        <div>{this.getMessage()}</div>
                    </Typography>
                    <Formik
                        initialValues={this.getInitialValues()}
                        validationSchema={validationSchema}
                        onSubmit={this.handleSubmit.bind(this)}
                        ref={node => this._form = node}
                    >
                        {props => {
                            return (
                                this.getForm(classes, props)
                            )
                        }}
                    </Formik>
                </React.Fragment>
            )
        }
    }
}

export default withStyles(styles)(DeviceForm);
