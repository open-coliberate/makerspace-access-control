import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import { withRouter } from 'react-router-dom';

import {
    Fab,
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper
} from '@material-ui/core'

import { Autorenew, Wifi, WifiOff } from '@material-ui/icons'

const styles = theme => ({
    center: {
        display: 'flex',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        direction: 'column',
        height: '100vh'
    },
    buttonGridItem: {
        marginBottom: '11px'
    },
    wifiInfo: {
        marginBottom: '44px',
        display: 'flex',
        alignItems: 'center'
    }
})

class WifiDashboard extends React.Component {
    state = {
        wifiConnectStarted: false,
        isConnected: false,
        ssid: 'Not-foo-bar'
    }

    componentWillMount() {
        this.updateCurrentWifiInfo()
    }

    updateCurrentWifiInfo = () => {
        fetch('http://localhost:8000/get-wifi-info', {
            method: 'POST'
        })
        .then(resp => resp.json())
        .then(data => {
            this.setState({
                isConnected: data.is_connected,
                ssid: data.ssid
            })
        })
        .catch((err) => {
            console.log(`Error retrieving wifi info ${err}`)
        })
    }

    startWifiConnect = () => {
        this.setState({wifiConnectStarted: true})
        fetch('http://localhost:8000/wifi-connect', {
            method: 'GET'
        })
        .then((resp) => { this.setState({wifiConnectStarted: false}) })
        .catch((err) => { this.setState({wifiConnectStarted: false}) })
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={3}>
                    </Grid>
                    <Grid item xs={6} className={classes.center} direction='column'>
                        <Grid item className={classes.wifiInfo}>
                            {this.state.isConnected ? <Wifi /> : <WifiOff />} &nbsp; {this.state.ssid || 'Not connected'}
                        </Grid>
                        <Grid item className={classes.buttonGridItem}>
                            <Fab color="primary" variant="extended" aria-label="Delete" className={classes.fab} onClick={this.startWifiConnect} disabled={this.state.wifiConnectStarted}>
                                <Autorenew className={classes.extendedIcon} />
                                Setup wifi
                            </Fab>
                        </Grid>

                        <Grid>
                            <Grid item>
                                <Paper >
                                    <List>
                                        <ListItem>
                                            <ListItemText
                                                primary="1. Connect to 'Wifi Connect'"
                                                secondary="Use another device, such as a cellphone or laptop, to connect to the wifi network with the name 'Wifi Connect'"
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText
                                                primary="2. Enter wifi credentials"
                                                secondary="Once connected to 'Wifi Connect' wait for a splash screen to appear which will allow you to enter your wifi credentials. It may take up to 1 minute for the portal to appear."
                                            />
                                        </ListItem>
                                    </List>
                                </Paper>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(withStyles(styles)(WifiDashboard))