import React from 'react';

import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import { withRouter } from 'react-router-dom';

import {
    Fab,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow
} from '@material-ui/core'

import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

import { Add } from '@material-ui/icons'

import UserEditFormDialog from './UserEditFormDialog'

const styles = theme => ({
    root: {
        flexGrow: 1,
        // padding: `0 ${theme.spacing.unit * 12}px`
        paddingTop: 20
    },
    titleBar: {
        paddingBottom: 5
    },
    tableWrapper: {
        height: 350,
        overflowY: 'scroll'
    },
    paper: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 2,
        // marginBottom: theme.spacing.unit * 2,
    },
    table: {
        height: '100%'
    },
    head: {
        top: 0,
        backgroundColor: "#fff",
        position: "sticky"
    },
    foot: {
        bottom: 0,
        backgroundColor: "#fff",
        position: "sticky"
    },
    fab: {
        margin: theme.spacing.unit,
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    }
})

const actionsStyles = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5,
    },
});

class TablePaginationActions extends React.Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
        );
    };

    render() {
        const { classes, count, page, rowsPerPage, theme } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }
}

TablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
    TablePaginationActions,
);

class Users extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [],
            rowsPerPage: 25,
            page: 0,
            count: 0,
            error: null
        }
    }

    async componentWillMount() {
        await this.getUsers(0)
    }

    updateUserData = () => {
        this.getUsers(this.state.page, null)
    }

    handleRowClick = (id) => {
        const user = this.state.users.find((user) => user.id === id);
        console.log('Showing edit form for user:', user)
        this.editUserForm.handleClickOpen(user)
    }

    addUser = () => {
        console.log('Create user form open')
        this.editUserForm.handleClickOpen({})
    }

    handleChangePage = async (event, page) => {
        this.setState({ page });
        await this.getUsers(page)
    };

    handleChangeRowsPerPage = async (event )=> {
        const rowsPerPage = parseInt(event.target.value)
        this.setState({ page: 0, rowsPerPage: rowsPerPage });
        // Pass rows per page since state will not have been updated yet..
        await this.getUsers(0, rowsPerPage)
    };

    getUsers = async (page, rowsPerPageOverride) => {
        const { rowsPerPage } = this.state;
        const finalRowsPerPage = rowsPerPageOverride || rowsPerPage
        const start = (page * finalRowsPerPage) || 0
        const resp = await fetch(`http://localhost:8000/user/list?start=${start}&size=${finalRowsPerPage}`)
        const data = await resp.json()
        this.setState({ users: data.users, count: data.count })
    }

    render() {
        const { classes } = this.props;
        const { rowsPerPage, page } = this.state;
        const rows = this.state.users;
        const emptyRows = rowsPerPage - rows.length;

        return (
            <div className={classes.root}>
                <UserEditFormDialog
                    ref={(node) => this.editUserForm = node}
                    onUserEditCompleted={this.updateUserData}
                />
                <Grid container spacing={24}>
                    <Grid item xs={1}>
                    </Grid>
                    <Grid item xs={10}>
                        <Grid item>
                            <Fab color="primary" variant="extended" aria-label="Delete" className={classes.fab} onClick={this.addUser}>
                                <Add className={classes.extendedIcon} />
                                Add user
                            </Fab>
                        </Grid>

                        <Grid item>
                            <Paper className={classes.tableWrapper}>
                                <Table className={classes.table}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell className={classes.head}>Email</TableCell>
                                            <TableCell className={classes.head} align="right">Administrator?</TableCell>
                                            <TableCell className={classes.head} align="right">Card set up?</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {rows.map(row => (
                                            <TableRow key={row.id} onClick={() => this.handleRowClick(row.id)}>
                                                <TableCell component="th" scope="row">
                                                    {row.email ? row.email : `${row.first_name} ${row.last_name}`}
                                                </TableCell>
                                                <TableCell align="center">{row.is_admin ? "yes" : "no"}</TableCell>
                                                <TableCell align="center">{row.card_uid ? "yes" : "no"}</TableCell>
                                            </TableRow>
                                        ))}
                                        {emptyRows > 0 && (
                                            <TableRow style={{ height: 48 * emptyRows }}>
                                                <TableCell colSpan={6} />
                                            </TableRow>
                                        )}
                                    </TableBody>
                                    <TableFooter>
                                        <TableRow>
                                            <TablePagination className={classes.foot}
                                                rowsPerPageOptions={[25, 50, 100]}
                                                colSpan={3}
                                                count={this.state.count}
                                                rowsPerPage={rowsPerPage}
                                                page={page}
                                                SelectProps={{
                                                    native: true,
                                                }}
                                                onChangePage={this.handleChangePage}
                                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                                ActionsComponent={TablePaginationActionsWrapped}
                                            />
                                        </TableRow>
                                    </TableFooter>
                                </Table>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Grid item xs={2}>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(withStyles(styles)(Users))
