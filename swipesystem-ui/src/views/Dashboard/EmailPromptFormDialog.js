import React from 'react';
import { Formik } from 'formik'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import * as Yup from "yup";

const validationSchema = Yup.object().shape({
    email: Yup.string().email().required("Required field"),
})

export default class FormDialog extends React.Component {
    state = {
        open: false,
        cardUid: null
    };

    handleSwipeEvent = (cardUid) => {
        this.setState({
            open: true,
            cardUid: cardUid 
        });
    };

    handleClose = () => {
        this.setState({ open: false });
    }
    
    handleSubmit = async (data) => {
        this._form.submitForm()
    }

    onSubmit = async (data) => {
        data = {
            ...data,
            update: true,
            cardUid: this.state.cardUid
        }

        try {
            const resp = await fetch('http://localhost:8000/user/', {
                method: "POST",
                body: JSON.stringify(data)
            })
            const result = await resp.json()
            console.log('Updated user email', result)
            this.handleClose()
        } catch(error) {
            console.log('failed to submit', error)
        }
    }

    getForm(classes, props) {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;
        return (
            <form>
                <TextField
                    id="email"
                    label="email"
                    error={touched.email && Boolean(errors.email)}
                    value={values.email}
                    helperText={touched.email ? errors.email : ""}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    margin="normal"
                    fullWidth
                />
            </form>
        )
    }

    render() {
        const { classes } = this.props;

        return (
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Please update your email</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Must provide email to to complete swiping in!
                    </DialogContentText>
                    <Formik
                        enableReinitialize
                        initialValues={this.state.user}
                        validationSchema={validationSchema}
                        onSubmit={this.onSubmit}
                        ref={node => { this._form = node }}
                    >
                        {props => {
                            return (
                                this.getForm(classes, props)
                            )
                        }}
                    </Formik>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSubmit} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}