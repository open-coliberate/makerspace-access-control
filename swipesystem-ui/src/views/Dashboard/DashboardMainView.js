import React from 'react';
import { connect } from 'react-redux';
import io from 'socket.io-client';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import ReservationFormDialog from './ReservationFormDialog';
import EmailPromptFormDialog from './EmailPromptFormDialog';

import SwipeEventLock from '../../SwipeEventLock';


const styles = theme => ({
    root: {
        width: '100%',
        height: '100vh',
        background: '#f2f2f2',
        textAlign: 'left'
    },
    grid: {
        paddingLeft: '20px',
        paddingRight: '20px',
    },
    paper: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 2,
        margin: theme.spacing.unit * 2,
    },
    mm: {
        display: 'flex',
        justifyContent: 'space-between'
    }
});


class DashboardMainView extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isReservationFormDialogOpen: false,
            isEmailPromptOpen: false,
            timezone: props.settings.timezone,
            clock: new Date().toLocaleTimeString('en-US', { timeZone: props.settings.timezone }),
            date: new Date().toLocaleDateString('en-US', { timeZone: props.settings.timezone }),
            nickName: "Main 3D Printer",
            space: "Columbia makerspace demo",
            intervalId: null,
            cardUid: null,
            reservation: {
                user: null,
                cardUid: null,
                startTime: null,
                durationMins: null,
                endTime: null
            },
        }
    }


    componentWillReceiveProps(props) {
        this.setState({timezone: props.settings.timezone});
    }

    async getSettings() {
        const resp = await fetch('http://localhost:8000/update-settings')
        const data = await resp.json()
        if (data.timezone !== this.state.timezone) {
            this.props.updateTimezone({ timezone: data.timezone })
                .then((result) => {
                    this.setState({timezone: data.timezone})
                    console.log('update timezone',data)
                })
                .catch(() => {
                    console.error("Error getting settings")
                })
        }
        return true
    }

    componentWillMount() {
        this.getSettings()
        const intervalId = setInterval(() => {
            this.setState({
                clock: new Date().toLocaleTimeString('en-US', { timeZone: this.state.timezone }),
                date: new Date().toLocaleDateString('en-US', { timeZone: this.state.timezone })
            })
        }, 1000)
        this.setState({intervalId: intervalId})
    }


    async shouldPromptForEmail(cardUid) {
        const resp = await fetch(`http://localhost:8000/user-has-email?cardUid=${cardUid}`)
        const data = await resp.json()
        if (data.has_email === true || data.user_exists === false) {
            return false
        }
        return true
    }


    async notifyOfSwipeEvent(cardUid) {
        console.log('got swipe event', cardUid)
        const showEmailPrompt = await this.shouldPromptForEmail(cardUid)
        if (showEmailPrompt) {
            console.log('User email is missing')
            this.emailPromptForm.handleSwipeEvent(cardUid)
        } else {
            this.dialogForm.handleSwipeEvent(cardUid)
            this.setState({isReservationFormDialogOpen: true})
        }
    }


    setupSocket() {
        console.log('Opening socket')
        this.socket = io('http://localhost:8000/feed')
        this.socket.on('data', async (event) => {
            if (event.type === 'card-swipe' && !SwipeEventLock.isLocked()) {
                console.log('got card swipe', event)
                const cardUid = event.payload.card_id.replace('\n', '')
                this.setState({cardUid: cardUid})
                await this.notifyOfSwipeEvent(cardUid)
            }
        })
    }


    updateReservationState(reservation) {
        this.setState({reservation: reservation})
    }


    async componentDidMount() {
        this.setupSocket()
        console.log(this.props)
    }


    componentWillUnmount() {
        clearInterval(this.state.intervalId)
        this.setState({intervalId: null})
        this.socket.close();
    }


    closeReservationDialog = () => {
        this.setState({ isReservationFormDialogOpen: false });
    };


    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
            <EmailPromptFormDialog
                close={() => console.log('TODO impl close')}
                ref={node => this.emailPromptForm = node}
            />
            <ReservationFormDialog
                ref={node => this.dialogForm = node}
                open={this.state.isReservationFormDialogOpen}
                closeHandler={this.closeReservationDialog}
                reservationChangeHandler={this.updateReservationState.bind(this)}
                cardUid={this.state.cardUid}
            />

            <Grid container spacing={0} className={classes.grid}>
                <Grid item xs={12} >
                    <Paper className={classes.paper} elevation={1} square={true}>
                        <div className={classes.mm}>
                            <div><Typography variant="h5">
                                {this.state.nickName}</Typography></div>
                            <div><Typography variant="h5">
                                {this.state.clock}</Typography></div>
                        </div>
                        <div className={classes.mm}>
                            <div><Typography variant="subtitle1">
                                {this.state.space}</Typography></div>
                            <div><Typography variant="subtitle1">
                                {this.state.date}</Typography></div>
                        </div>
                    </Paper>
                </Grid>
                <Grid item xs={6} >
                    <Paper className={classes.paper} elevation={1} square={true}>
                            {this.state.reservation.user ?
                            ("Occupied by " + this.state.reservation.user) :
                            'Swipe to reserve'}
                        <Typography component="p">
                                {this.state.reservation.user ?
                                ("Occupied until " + this.state.reservation.endTimeHHMMA) :
                                "Currently available for use"}
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={6} >
                    <Paper className={classes.paper} elevation={1} square={true}>
                        <Typography variant="h5" component="h3">
                        Schedule
                        </Typography>
                        <Typography component="p">
                        &#8226; Salah C. 2:30 PM to 3:30 PM
                        </Typography>
                        <Typography component="p">
                        &#8226; Jack B. 3:30 PM to 4:00 PM
                        </Typography>
                    </Paper>
                </Grid>
            </Grid >
            </div>
        )
    }
}

export default connect(
    (state) => ({
        settings: state.settings
    })
)(withStyles(styles)(DashboardMainView))
