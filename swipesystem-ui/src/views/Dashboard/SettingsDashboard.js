import React from 'react';
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles';
import {
    Button,
} from '@material-ui/core'

import TimezonePicker from 'react-timezone';
import SimpleSnackbar from '../../components/snack-bar'

const styles = theme => ({
    center: {
        display: 'flex',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        direction: 'column',
        height: '100vh'
    },
    form: {
        display: 'flex',
        justifyContent: 'space-evenly',
        width: '70%'
    }
})

class SettingsDashboard extends React.Component {
    state = {
        timezone: null,
        msg: null
    }

    componentWillMount() {
        this.setState({timezone: this.props.settings.timezone})
        this.getSettings()
    }

    handleSubmit = () => {
        this.props.updateTimezone({ timezone: this.state.timezone })
            .then((result) => {
                this.setState({msg: "Settings updated"})
            })
            .catch(() => {
                this.setState({msg: "Error updating settings"})
            })
    }

    async getSettings() {
        const resp = await fetch('http://localhost:8000/update-settings')
        const data = await resp.json()
        if (data.timezone !== this.state.timezone) {
            this.props.updateTimezone({ timezone: data.timezone })
                .then((result) => {
                    this.setState({timezone: data.timezone})
                    console.log('update timezone',data)
                })
                .catch(() => {
                    console.error("Error getting settings")
                })
        }
        return true
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.center}>
                <div className={classes.form}>
                    <TimezonePicker
                        absolute={true}
                        value={this.state.timezone}
                        onChange={(timezone) => this.setState({ timezone })}
                        inputProps={{
                        placeholder: 'Select Timezone...',
                        name: 'timezone',
                        style: {
                            width: '150%'
                        }
                        }}
                    />
                    <br />
                    <br />

                    <Button variant="contained" color="primary" onClick={this.handleSubmit}>
                        Save
                    </Button>
                    {
                        this.state.msg ? (
                            <SimpleSnackbar msg={this.state.msg} handleClose={() => this.setState({msg: null})} />
                        ) :
                        null
                    }
                </div>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        settings: state.settings
    }),
)(withStyles(styles)(SettingsDashboard))
