import React from 'react';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';


import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';


const getTimeHHMMAFromDateStr = (date) => {
    if (date) {
        const endTime = new Date(date)
        return endTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
    }
    return null
}


const getDate = (date) => {
    if (date) {
        return new Date(date)
    }
}


export default class ReservationFormDialog extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reservation: {
                user: null,
                cardUid: null,
                startTime: null,
                endTime: null,
            },
            showReswipeForm: false,
            reservationWelcome: "20 minutes",
            reservationError: ""
        };

        this.handleClose = this.props.closeHandler;
    }


    componentDidMount() {
        this.refreshReservationInfo()
    }


    handleChange = event => {
        this.setState({ value: parseInt(event.target.value) });
    }


    handleExit() {
        this.setState({showReswipeForm: false, reservationError: "", reservationWelcome: ""})
    }


    async getCurrentReservation() {
        const getCall = await fetch('http://localhost:8000/tool-reservation', { method: 'GET' })
        return await getCall.json()
    }


    async handleExtendReservationClick() {
        const postCall = await fetch('http://localhost:8000/tool-reservation', {
            method: 'POST',
            body: JSON.stringify({
                cardUid: this.state.reservation.cardUid,
                action: 'extend'
            })
        })

        await postCall.json()  // TODO: check for error?

        this.setState({
            reservationWelcome: "Your reservation has been extendled.",
        })

        await this.refreshReservationInfo()
    }


    async handleCancelReservationClick() {
        const postCall = await fetch('http://localhost:8000/tool-reservation', {
            method: 'POST',
            body: JSON.stringify({
                cardUid: this.state.reservation.cardUid,
                action: 'cancel'
            })
        })

        await postCall.json()  // TODO: check for error?
        await this.handleOutputStatus({outputStatus: false})

        this.setState({
            reservationWelcome: "Your reservation has been cancelled.",
            showReswipeForm: false
        })

        await this.refreshReservationInfo()
    }


    async createReservation(cardUid) {
        const postCall = await fetch('http://localhost:8000/tool-reservation', {
            method: 'POST',
            body: JSON.stringify({
                cardUid: cardUid,
                action: 'create'
            })
        })
        const postResp = await postCall.json()
        return postResp
    }


    async handleOutputStatus(data) {
        try {
            const resp = await fetch("http://localhost:8000/update-output", {
                method: "POST",
                body: JSON.stringify(data)
            })
            const result = await resp.json()
            console.log("handleOutputStatus", result)
            return result
        } catch (error) {
            console.log('failed to switch output', error)
        }
    }


    refreshReservationWhenReservationEnds = (endTime) => {
        if (this.reservationTimer) {
            clearTimeout(this.reservationTimer) // remove previous timers if any
        }

        const durationMs = (endTime - new Date())
        if (endTime === null || endTime === undefined || (durationMs <= 0)) {
            return
        }

        const that = this
        this.reservationTimer = setTimeout(() => {
            that.refreshReservationInfo()
            console.log('Reservation refreshed by timeout timer')
            this.handleOutputStatus({outputStatus: false})
        }, durationMs)
        console.log('Set up rervation timer for', durationMs / 1000)
    }


    async refreshReservationInfo() {
        const res = await this.getCurrentReservation()
        const reservation = {
            user: res.user,
            cardUid: res.card_uid,
            durationMins: res.durationMins,
            startTime: getDate(res.start_time),
            endTime: getDate(res.end_time),
            endTimeHHMMA: getTimeHHMMAFromDateStr(res.end_time),
        }
        this.setState({ reservation: reservation })
        this.refreshReservationWhenReservationEnds(reservation.endTime)
        this.props.reservationChangeHandler(reservation)
        console.log('reservation info refreshed', reservation)
    }


    async handleSwipeEvent(cardUid) {
        this.setState({ isLoading: true })
        try {
            const currentReservation = await this.getCurrentReservation()
            if (currentReservation.card_uid === cardUid) {
                console.log('reswipe detected...')
                const endTimeHHMMA = getTimeHHMMAFromDateStr(currentReservation.end_time)
                this.setState({
                    reservationWelcome: `Your current reservation ends at ${endTimeHHMMA}`,
                    showReswipeForm: true
                })
                return
            }
            const postResp = await this.createReservation(cardUid)

            if (postResp.status !== 'success') {
                this.setState({
                    isLoading: false,
                    reservationError: postResp.error
                })
            } else {
                await this.handleOutputStatus({outputStatus: true})
                await this.refreshReservationInfo()
                this.setState({
                    reservationWelcome: `Reservation started. Your current reservation ends at ${this.state.reservation.endTimeHHMMA}`
                })

            }

        } catch (err) {
            console.log('failed to reserve tool', err)
            this.setState({ reservationError: err.message })
        }
        this.setState({ isLoading: false })
    }


    getReswipeForm() {
        return (
            <DialogActions>
                <div style={{ display: 'flex', alignContent: 'space-around' }}>
                    <Button variant="outlined" color="primary" size="small" onClick={this.handleExtendReservationClick.bind(this)}>
                        Extend reservation
                    </Button>
                    <Button variant="outlined" color="secondary" size="small" onClick={this.handleCancelReservationClick.bind(this)}>
                        Cancel reservation
                    </Button>
                </div>
            </DialogActions>
        )
    }


    getReservationResult() {
        return (
            <div style={{ textAlign: 'center', display: 'flex' }}>
                {this.state.reservationError ? this.state.reservationError : this.state.reservationWelcome}
            </div>
        )
    }


    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.props.closeHandler}
                onExit={this.handleExit.bind(this)}
                PaperProps={{
                    style: {
                        height: '260px',
                        width: '340px'
                    },
                }}
            >
                <DialogTitle id="form-dialog-title" style={{ textAlign: 'center' }}>
                    {this.state.reservationError ? "Sorry..." : "Welcome " + this.state.reservation.user}
                </DialogTitle>
                <DialogContent
                    style={{ backgroundColor: '#e6ffee', justifyContent: 'space-evenly' }}
                >
                    {this.getReservationResult()}
                </DialogContent>
                    {this.state.showReswipeForm && this.getReswipeForm()}
            </Dialog>
        )
    }
}
