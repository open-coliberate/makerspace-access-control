import React from 'react';
import { Formik } from 'formik'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { Checkbox, FormControlLabel } from '@material-ui/core'

import * as Yup from "yup";

import io from 'socket.io-client'

const validationSchema = Yup.object().shape({
    email: Yup.string().email().required("Required field"),
})


export default class FormDialog extends React.Component {
    state = {
        open: false,
        user: null
    };

    updateCardUid(cardUid) {
        this.setState({cardUid: cardUid})
    }

    componentDidMount() {
        console.log('Opening socket')
        this.socket = io('http://localhost:8000/feed')
        this.socket.on('data', (event) => {
            if (event.type === 'card-swipe') {
                this.updateCardUid(event.payload.card_id)
            }
        })
    }

    componentWillUnmount() {
        console.log('Closing socket')
        this.socket.close();
    }

    handleClickOpen = (user) => {
        console.log('user is', user)
        this.setState({
            open: true,
            cardUid: user.card_uid,
            user: {
                id: user.id,
                email: user.email,
                isAdmin: user.is_admin
            }
        });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleComplete = () => {
        this.props.onUserEditCompleted()
        this.handleClose()
    }
    
    handleSubmit = async (data) => {
        this._form.submitForm()
    }

    onSubmit = async (data) => {
        data = {...data, cardUid: this.state.cardUid}
        try {
            const resp = await fetch('http://localhost:8000/user/', {
                method: "POST",
                body: JSON.stringify(data)
            })
            console.log(await resp.json())
            console.log('done')
            this.handleComplete()
        } catch(error) {
            console.log('failed to submit', error)
        }
    }

    getForm(classes, props) {
        const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
        } = props;
        return (
            <form>
                <TextField
                    id="email"
                    label="email"
                    // className={classes.textField}
                    error={touched.email && Boolean(errors.email)}
                    value={values.email}
                    helperText={touched.email ? errors.email : ""}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    margin="normal"
                    fullWidth
                />
                <div style={{display: 'flex', alignContent: 'space-around', width: 530}}>
                {/* <TextField
                    id="firstName"
                    label="First name"
                    // className={classes.textField}
                    error={touched.firstName && Boolean(errors.firstName)}
                    value={values.firstName}
                    helperText={touched.firstName ? errors.firstName : ""}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    margin="normal"
                    fullWidth
                />
                <TextField
                    id="lastName"
                    label="Last name"
                    // className={classes.textField}
                    error={touched.lastName && Boolean(errors.lastName)}
                    value={values.lastName}
                    helperText={touched.lastName ? errors.lastName : ""}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    margin="normal"
                    fullWidth
                /> */}
                </div>
                <div style={{display: 'flex', alignContent: 'space-around', width: 530}}>
                <TextField
                    disabled
                    id="cardUid"
                    // className={classNames(classes.margin, classes.textField)}
                    type='password'
                    label="Card ID"
                    helperText="Swipe your card on the card reader"
                    value={this.state.cardUid}
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                id="isAdmin"
                                checked={values.isAdmin ? true : false}
                                onChange={handleChange}
                                color="primary"
                            />
                        }
                        label="Admin user?"
                    />
                </div>
            </form>
        )
    }

    render() {
        const { classes } = this.props;

        return (
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Edit User</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Fill out user info.
                    </DialogContentText>
                    <Formik
                        enableReinitialize
                        initialValues={this.state.user}
                        validationSchema={validationSchema}
                        onSubmit={this.onSubmit}
                        ref={node => { this._form = node }}
                    >
                        {props => {
                            return (
                                this.getForm(classes, props)
                            )
                        }}
                    </Formik>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSubmit} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}