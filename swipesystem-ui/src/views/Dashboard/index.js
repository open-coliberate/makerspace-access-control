import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import DashboardMainViewContainer from '../../containers/DashboardMainViewContainer'
import {Route, Switch, Link } from 'react-router-dom'

import Drawer from '@material-ui/core/Drawer';
import { MenuItem, MenuList } from '../../../node_modules/@material-ui/core';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Users from './Users';
import WifiDashboard from './WifiDashboard';
import ToolProfileDashboard from './ToolProfileDashboard';
import ReservationHistory from './ReservationHistory';
import SettingsDashboardContainer from '../../containers/SettingsDashboardContainer'

import { Build, People, Settings, Timeline, Home, Wifi} from '@material-ui/icons'
import HasAdminPermissionsPromptDialog from './HasAdminPermissionsPromptDialog';


const styles = theme => ({
    root: {
        width: '100%',
        height: '100vh',
        background: '#f2f2f2',
        textAlign: 'left'
    },
    grid: {
        paddingLeft: '20px',
        paddingRight: '20px',
    },
    paper: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 2,
        margin: theme.spacing.unit * 2,
    },
    mm: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    list: {
        width: 200
    },
    absolute: {
        fontSize: 40,
        position: 'absolute',
        bottom: theme.spacing.unit * 3,
        right: theme.spacing.unit * 3,
    }
});


class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menuOpen: false
        }

        this.adminPermissionsPrompt = React.createRef()
    }

    closeMenu = () => {
        this.setState({menuOpen: false})
    }

    toggleMenu = () => {
        if (window.location.pathname === '/') {
            this.adminPermissionsPrompt.current.open()
        } else {
            this.setState({menuOpen: !this.state.menuOpen})
        }
    }

    handleValidAdminUser = () => {
        this.setState({menuOpen: !this.state.menuOpen})
    }

    handleInValidAdminUser = () => {
        // Alert user of insufficient permissions
        alert('Only admin users are allowed to access the sidebar')
    }

    redirect = () => {
    }

    render() {
        console.log(this.constructor.name)
        const { classes } = this.props;

        const sideList = (
            <div className={classes.list}>
                <MenuList>
                    <MenuItem component={Link} to="/" className={classes.menuItem}>
                        <ListItemIcon><Home /></ListItemIcon>
                        <ListItemText primary="Home page" />
                    </MenuItem>
                    <MenuItem component={Link} to="/users" className={classes.menuItem}>
                        <ListItemIcon><People /></ListItemIcon>
                        <ListItemText primary="Users" />
                    </MenuItem>
                    <MenuItem component={Link} to="/history" className={classes.menuItem}>
                        <ListItemIcon><Timeline /></ListItemIcon>
                        <ListItemText primary="History" />
                    </MenuItem>
                    <MenuItem component={Link} to="/toolprofile" className={classes.menuItem}>
                        <ListItemIcon><Build /></ListItemIcon>
                        <ListItemText primary="Tool profile" />
                    </MenuItem>
                    <MenuItem component={Link} to="/wifi" className={classes.menuItem}>
                        <ListItemIcon><Wifi /></ListItemIcon>
                        <ListItemText primary="Wifi" />
                    </MenuItem>
                    <MenuItem component={Link} to="/settings" className={classes.menuItem}>
                        <ListItemIcon><Settings /></ListItemIcon>
                        <ListItemText primary="Settings" />
                    </MenuItem>
                </MenuList>
            </div>
        );

        return (
            <div className={classes.root}>
                <Drawer
                    open={this.state.menuOpen}
                    onClose={this.closeMenu}
                    >
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.closeMenu}
                        onKeyDown={this.closeMenu}
                    >
                        {sideList}
                    </div>
                </Drawer>

                <Switch>
                    <Route exact path="/" component={DashboardMainViewContainer}/>
                    <Route path="/users" component={Users} />
                    <Route path="/wifi" component={WifiDashboard} />
                    <Route path="/history" component={ReservationHistory} />
                    <Route path="/toolprofile" component={ToolProfileDashboard} />
                    <Route path="/settings" component={SettingsDashboardContainer} />
                </Switch>
                <HasAdminPermissionsPromptDialog
                    ref={this.adminPermissionsPrompt}
                    handleValidAdminUser={this.handleValidAdminUser}
                    handleInValidAdminUser={this.handleInValidAdminUser}
                />
                <Settings onClick={this.toggleMenu} className={classes.absolute} />
            </div>
        )
    }
}

export default withStyles(styles)(Dashboard)
