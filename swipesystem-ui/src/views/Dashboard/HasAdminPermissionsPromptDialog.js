import React from 'react';
import io from 'socket.io-client'

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import SwipeEventLock from '../../SwipeEventLock'

export default class HasAdminPermissionsPromptDialog extends React.Component {
    state = {
        open: false
    };

    setupSocket() {
        console.log('Opening socket')
        this.socket = io('http://localhost:8000/feed')
        this.socket.on('data', async (event) => {
            if (event.type === 'card-swipe' && SwipeEventLock.isLockedBy(this.constructor.name)) {
                console.log('got card swipe', event)
                const cardUid = event.payload.card_id.replace('\n', '')
                this.setState({cardUid: cardUid})
                await this.checkCardUidPermissions(cardUid)
            }
        })

    }

    checkCardUidPermissions = async (cardUid) => {
        try {
            const resp = await fetch('http://localhost:8000/has-admin-permissions', {
                method: 'POST',
                body: JSON.stringify({cardUid: cardUid})
            })
            const jsonResp = await resp.json()
            console.log('resonse is', jsonResp)
            if (jsonResp.result === true) {
                this.handleValidAdminUser()
            } else {
                this.handleInValidAdminUser()
            }

        } catch(err) {
            alert(`Error reaching backend system: ${err}`)
        }
    }

    open = () => {
        SwipeEventLock.obtainLock(this.constructor.name)
        this.setState({ open: true })
        this.setupSocket()
    };

    handleValidAdminUser = () => {
        console.log('Handle valid admin user')
        this.handleClose()
        this.props.handleValidAdminUser()
    }

    handleInValidAdminUser = () => {
        console.log('Handle invalid admin user')
        this.handleClose()
        this.props.handleInValidAdminUser()
    }

    handleClose = () => {
        this.setState({ open: false });
        this.socket.close();
        this.socket = null; // destory socket reference
        SwipeEventLock.releaseLock()
    };

    render() {
        return (
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Admin user required</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        You must be an admin user to access settings. Please swipe your card now to confirm permissions.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}