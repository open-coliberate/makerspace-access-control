import React from 'react'

import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

import ToolProfileForm from '../../forms/ToolprofileForm'

import {
    Button,
    Grid,
    Paper,
    Typography
} from '@material-ui/core';
import AlertDialog from '../../components/AlertDialog';


const styles = theme => ({
    textFieldD: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    root: {
        display: 'flex',
    },
    paper: {
      paddingTop: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 2,
      height: 240,
    },
    formControl: {
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * -3,
      marginLeft: theme.spacing.unit * 5,
    },
    formControl2: {
      marginTop: theme.spacing.unit * -2,
      marginBottom: theme.spacing.unit * -3,
      marginLeft: theme.spacing.unit * 8,
    },
    grid: {
        alignContent: 'space-around',
        height: '480px',
        justifyContent: 'space-between'
    },
    buttonRow: {
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: 'inherit'
    },
    button: {
        marginTop: theme.spacing.unit * 2
    }
});

class ToolProfileDashboard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            initialValues: {
                profileName: "",
                powerOnSeconds: "",
                warnAtSeconds: "",
                requiresTraining: false,
                restrictedToOpenHours: false,
                enableWarningSound: false,
                buddyMode: false,
            },
            deviceId: "",
            newProfile: false,
            open: false,
            profiles: [],
            message: "",
            alertOpen: false,
            alertTitle: "",
            alertText: "",
        };
    }

    getInitialData = async () => {
        const resp = await fetch('http://localhost:8000/tool-profiles/')
        const respJson = await resp.json()
        this.setState({initialValues: respJson.data})
        console.log('got resp', this.state.initialValues)
    }

    async componentWillMount() {
        await this.getInitialData()
    }

    submitForm = () => {
        this.form.submitForm()
    }

    openAlert = (alertTitle, alertText) => {
        this.setState({alertOpen: true, alertTitle: alertTitle, alertText: alertText})
    }

    closeAlert = () => {
        this.setState({alertOpen: false})
    }

    handleSubmit = async (formData) => {
        console.log('Submitting tool profile form', formData)
        const resp = await fetch('http://localhost:8000/tool-profiles/', {
            method: 'POST',
            body: JSON.stringify(formData)
        })
        const res = await resp.json()
        if (res.status === "success") {
            this.openAlert("Success", "Profile update completed successfully")
        } else {
            this.openAlert("Error", res.error)
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <Grid container spacing={24}>
                <Grid item xs={1}>
                <AlertDialog
                    open={this.state.alertOpen}
                    text={this.state.alertText}
                    title={this.state.alertTitle}
                    handleClose={this.closeAlert} />
                </Grid>
                <Grid item xs={10} className={classes.grid}>
                    <Grid item>
                        <Typography variant="h4" gutterBottom>
                            Tool profile settings
                        </Typography>
                    </Grid>
                    <Grid item>
                    <Paper className={classes.paper}>
                        <ToolProfileForm
                            innerRef={node => this.form = node}
                            initialValues={this.state.initialValues}
                            handleSubmit={this.handleSubmit} />
                    </Paper>
                    </Grid>
                    <Grid item className={classes.buttonRow}>
                        <Button
                        variant="contained"
                        color="primary"
                        onClick={this.submitForm}
                        className={classes.button} >
                            Submit
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default withRouter(withStyles(styles)(ToolProfileDashboard))
