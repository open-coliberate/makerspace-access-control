class SwipeEventLock {
    constructor() {
        this.lockOwner = null
    }

    isLocked() {
        return this.lockOwner === null ? false : true
    }

    isLockedBy(lockOwner) {
        return this.lockOwner === lockOwner
    }

    obtainLock(lockOwner) {
        console.log('Locking SwipeEventLock for', lockOwner)
        this.lockOwner = lockOwner
    }

    releaseLock() {
        console.log('Releasing SwipeEventLock from', this.lockOwner)
        this.lockOwner = null
    }
}

/**
 * This is is a singleton object used to determine if swipe event reads
 * are locked by a section of the applicaiton.
 * 
 * E.g.
 * 
 * On the device dashboard there are 2 application sections that are powered off
 * of swipe reading -- the reservation feature and the admin permission panel to access
 * the user side bar.
 * 
 * When the admin permissions modal is active it should intercept card read events
 * and not let the reservation feature use them.
 * 
 * In the future this should go away as locks are messy.
 * 
 */
const lock = new SwipeEventLock()

export default lock;
