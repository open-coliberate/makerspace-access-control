const DEFAULT_TIMEZONE = 'America/New_York';

const initialState = {
    settings: {
        timezone: DEFAULT_TIMEZONE
    }
}

export default function rootReducer(state = initialState, { type, payload, error, meta }) {
    switch (type) {
        case 'UPDATE_TIMEZONE':
            return {
                ...state,
                settings: {
                    ...state.settings,
                    timezone: payload.timezone || DEFAULT_TIMEZONE
                }
            }
        default:
            return state;
    }
}
