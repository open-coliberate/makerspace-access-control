export const updateTimezone = (payload) => (dispatch) => {
    return fetch('http://localhost:8000/update-settings', {
        method: "POST",
        body: JSON.stringify({timezone: payload.timezone})
    })
    .then(response => response.json())
    .then((resp) => {
        // TODO: assert resp is valid
        // TODO: update timezone
        dispatch({
            type: 'UPDATE_TIMEZONE',
            payload: {
                timezone: payload.timezone
            }
        })
    })

}
