# Makerspace Access Control

A security system to control who can use various tools in your Makerspace.

For example, you can allow some users to power on the drill
press, and other users can power on the soldering iron.  We also provide
ability to record "logins" when a user, customer or student enters
the space.

Highly secure, very low maintenance, customizable and it just works.


## Features

- Automated backups and easy restore
- Distributed, multi-device application
- Multiple RFID and card scanner options
- Support for multiple tool profiles
- Simple software updates

Please contact us if interested in being an early partner.  We currently sell
pre-built and ready-to-use packages.

`info@coliberate.io`


## Components:

### Hardware:

- Each Swipe Display Tool:

  - Raspberry Pi, 7" Touch Screen Display
  - Switch Box: AC/DC Relay connecting mains power (i.e. the wall) to your tool
  - RFID or card reader (other peripherals will be supported soon)
  - Enclosure box


### Software:

- `swipesystem-ui` - a [React](https://reactjs.org/) application built with
  [Material-UI](https://material-ui.com/).
- `swipesystem` - a python webserver that runs on the Pi.

Using the [balenaCloud](https://www.balena.io/) and
[balenaOS](https://www.balena.io/os/) to deploy and manage
[Dockerized](https://docs.docker.com/engine/examples/) a network of Swipe devices

#### System structure detailed [here](https://gitlab.com/CoLiberate/swipe-system/documentation/blob/extensions_structure/swipe-system-structure-doc.md).

#### For information on local development see [dev_setup.md](dev_setup.md).


## Credits:

Development is in partnership with Columbia Makerspace.
We test our prototypes there.
